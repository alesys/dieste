var $j = jQuery.noConflict();

var dynamicalHeight = (function () {
	var dynamicClasses = {
		1:'_one',2:'_two',3:'_three',
		4:'_four',5:'_five',6:'_six',
		7:'_seven',8:'_eight',9:'_nine',
		10:'_ten',11:'_eleven',12:'_twelve'
	}
	var dynamicClass = 'dynamic_height';
	var addDynamicHeight = function(){
		$j('.blog_holder > article > .post_content_holder > .post_text > .post_text_inner > h2').each(function() {
		  var t_height = $j(this).height();
		  var f_size = parseInt($j(this).first().css('font-size'),10) + 5;
		  var result = Math.floor(t_height/f_size + 1);
		  $j(this).next().removeClass();
		  $j(this).next().addClass(dynamicClass + dynamicClasses[result]);
		});
	};
	var resizeReloadClasses = function(){
			$j( window ).resize(function() {
			  	addDynamicHeight();
			  	console.log('resize');
			});
	};
	return {
		addDynamicHeight:addDynamicHeight,
		resizeReloadClasses: resizeReloadClasses
	}
})();

var portfolioSliderCustom = (function () {

	var bigSlider 	= 	$j( ".portfolio_slides:eq( 0 )" );
	var secondSlider =  $j( ".portfolio_slides:eq( 1 )" );
	var thirdSlider =   $j( ".portfolio_slides:eq( 2 )" );

	var addClickEventToSliderOverlay = function(){
		$j( ".portfolio_slides:eq( 0 ) > li > .image_text_zoom_hover" ).each(function() {
		  $j( this ).click(function() {
		  		console.log('Click');
		  		window.location.href = $j(this).children().children().children().children().children().attr('href');
		   });
		});
	};

	var stopSliders = function(){
		bigSlider.trigger('stop');
		$j( ".portfolio_slides:eq( 1 ),.portfolio_slides:eq( 2 )" ).trigger('stop');	
	};

	var startBig = function(){
		bigSlider.trigger('play',true);
		$j('#sliderOverlayBig').hide();
	};

	var disableSmall = function(){
		$j('#sliderOverlaySmall').show();
	};

	var disableBig = function(){
		$j('#sliderOverlayBig').show();
	};

	var startSmall = function(){
		bigSlider.trigger('stop');
		$j('#sliderOverlayBig').show();
	};

	var addOverlay = function(){
		bigSlider.parent().parent().parent().append(
			'<div id="sliderOverlayBig" style="z-index: 99999; background: rgba(82, 190, 235, 0.8); position: absolute; left:0; top:0; width: 100%; height: 100%"><p>Leadership</p></div>'
			);
	};

	var addOverlayFunctions = function(){
		bigSlider.parent().parent().parent().hover(startBig, startSmall);
	}


	var fixSliders = function(){
		addOverlay();
		addOverlayFunctions();
		addClickEventToSliderOverlay();
	};

	return {
		fixSliders 	: fixSliders,
	}

})();

$j(document).ready(function() {
	"use strict";

	function headerColors() {
		var innerContent = $j(".title_subtitle_holder h1 span").text();

		switch (innerContent) {
			case 'brands':
				$j(".title_subtitle_holder h1 span").css('color', '#606060');
				break;
			case 'culture':
				$j(".title_subtitle_holder h1 span").css('color', '#ea148c');
				break;
			case 'entertainment':
				$j(".title_subtitle_holder h1 span").css('color', '#82c14a');
				break;
			case 'ideas':
				$j(".title_subtitle_holder h1 span").css('color', '#ad0a50');
				break;
			case 'provoke weekly':
				$j(".title_subtitle_holder h1 span").css('color', '#003d79');
				break;
			case 'tech':
				$j(".title_subtitle_holder h1 span").css('color', '#4cc8f2');
				break;
			default:
				$j(".title_subtitle_holder h1 span").css('color', '#606060');
				break;
		}
	}

	headerColors();

	$j('body').on('click', 'li.cat-item a', function() {
		setTimeout(function () {
		    headerColors();
		},2500);
	});

	$j('#provoke-menu > li > a ').each(function() {
		  if ($j(this).attr('href') == window.location.href){
		  	$j(this).parent().addClass('active');
		  }
		  
	});

	// selected option when colapse categories menu
	$j('#tinynav1 option').each(function(){
		if($j(this).attr('value') == window.location.href){
			$j(this).attr('selected', 'selected');
		};
	})
});


