'user strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    browserSync = require('browser-sync').create(),
    reload      = browserSync.reload;

gulp.task('sass', function () {
  gulp.src('styles/**/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(rename("main.min.css"))
    .pipe(gulp.dest('../backend/project/wp-content/themes/borderland-child/css/'))
    .pipe(reload({stream:true}));
});

gulp.task('compress', function() {
  gulp.src('js/default.js')
    .pipe(uglify())
    .pipe(rename("default.min.js"))
    .pipe(gulp.dest('../backend/project/wp-content/themes/borderland-child/js/'));

  return gulp.src('js/custom.js')
    .pipe(uglify())
    .pipe(rename("custom.min.js"))
    .pipe(gulp.dest('../backend/project/wp-content/themes/borderland-child/js/'));
});


gulp.task('sass:watch', function () {
  gulp.watch('styles/**/*.scss', ['sass']);
  gulp.watch('../backend/project/wp-content/themes/borderland-child/**/*', reload);
});

// gulp.task('browser-sync', function() {
//     browserSync.init({
//         proxy: "dev.dieste.com"
//     });
// });
gulp.task('watch', function() {
    gulp.watch('js/*.js', ['compress']);
});


gulp.task('default', [
        'sass:watch',
        'watch'
        // 'browser-sync'
]);
