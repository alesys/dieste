<?php

/***  Enqueue Elated child theme stylesheet ***/

function elated_child_enqueue_style() {
	wp_register_style( 'childstyle', get_stylesheet_directory_uri() . '/style.css'  );
	wp_register_style( 'childstyle-main', get_stylesheet_directory_uri() . '/css/main.min.css');
	wp_enqueue_style( 'childstyle' );
	wp_enqueue_style( 'childstyle-main' );
}
add_action( 'wp_enqueue_scripts', 'elated_child_enqueue_style', 11);

//if (!function_exists('elated_child_scripts')) {
	/**
	 * Function that includes all necessary scripts
	 */
	function elated_child_scripts() {
		global $wp_scripts;
		wp_dequeue_script('eltd_default');
		wp_enqueue_script("eltd_default", get_stylesheet_directory_uri()."/js/default.min.js", array(), false, true);
		wp_enqueue_script("eltd_child_custom_js", get_stylesheet_directory_uri()."/js/custom.min.js",array(),false,true);
		wp_dequeue_script('wpb_composer_front_js');
		wp_deregister_script('wpb_composer_front_js');
		wp_enqueue_script("wpb_composer_front_js", get_stylesheet_directory_uri()."/js/js_composer_front.js",array(),false,true);
		wp_register_script( 'wpb_composer_front_js', vc_asset_url( 'js/js_composer_front.js' ), array( 'jquery' ), WPB_VC_VERSION, true );

	}
	add_action('wp_enqueue_scripts', 'elated_child_scripts');
//}
//allow internal call to this server
function allow_server_mydomain( $allowed, $hostname, $url ) {
	//Allows local site prod.dieste.com for import
   if ( 'prod.dieste.com' == $hostname ) // IP resolves to something like 10.1.2.3
      return true; // Treat this server as being NOT internal network
   else
     return $allowed;
}
add_filter( 'http_request_host_is_external', 'allow_server_mydomain', 10, 3 );

/**
 * Categories widget class
 *
 * @since 2.8.0
 */
class WP_Widget_Categories_Child extends WP_Widget {

	public function __construct() {
		$widget_ops = array( 'classname' => 'widget_categories', 'description' => __( "A list or dropdown of categories." ) );
		parent::__construct('categories', __('Categories'), $widget_ops);
	}

	/**
	 * @staticvar bool $first_dropdown
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {

		static $first_dropdown = true;

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Categories' ) : $instance['title'], $instance, $this->id_base );

		$c = ! empty( $instance['count'] ) ? '1' : '0';
		$h = ! empty( $instance['hierarchical'] ) ? '1' : '0';
		$d = ! empty( $instance['dropdown'] ) ? '1' : '0';

		echo $args['before_widget'];
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		$cat_args = array(
			'orderby'      => 'name',
			'show_count'   => $c,
			'hierarchical' => $h
		);

		if ( $d ) {
			$dropdown_id = ( $first_dropdown ) ? 'cat' : "{$this->id_base}-dropdown-{$this->number}";
			$first_dropdown = false;

			echo '<label class="screen-reader-text" for="' . esc_attr( $dropdown_id ) . '">' . $title . '</label>';



			$cat_args['show_option_none'] = __( 'Select Category' );
			$cat_args['id'] = $dropdown_id;


			/**
			 * Filter the arguments for the Categories widget drop-down.
			 *
			 * @since 2.8.0
			 *
			 * @see wp_dropdown_categories()
			 *
			 * @param array $cat_args An array of Categories widget drop-down arguments.
			 */
			wp_dropdown_categories( apply_filters( 'widget_categories_dropdown_args', $cat_args ) );
?>

<script type='text/javascript'>
/* <![CDATA[ */
(function() {
	var dropdown = document.getElementById( "<?php echo esc_js( $dropdown_id ); ?>" );
	function onCatChange() {
		if ( dropdown.options[ dropdown.selectedIndex ].value > 0 ) {
			location.href = "<?php echo home_url(); ?>/?cat=" + dropdown.options[ dropdown.selectedIndex ].value;
		}
	}
	dropdown.onchange = onCatChange;
})();
/* ]]> */
</script>

<?php
		} else {
?>
		<ul>
<?php
		$cat_args['title_li'] = '';


		$categoryNews = get_category_by_slug( 'news' );

		if ($categoryNews){
			$cat_args['exclude'] = array($categoryNews->cat_ID);
		}

		/**
		 * Filter the arguments for the Categories widget.
		 *
		 * @since 2.8.0
		 *
		 * @param array $cat_args An array of Categories widget options.
		 */
		wp_list_categories( apply_filters( 'widget_categories_args', $cat_args ) );

?>
		</ul>
<?php
		}

		echo $args['after_widget'];
	}

	/**
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['count'] = !empty($new_instance['count']) ? 1 : 0;
		$instance['hierarchical'] = !empty($new_instance['hierarchical']) ? 1 : 0;
		$instance['dropdown'] = !empty($new_instance['dropdown']) ? 1 : 0;

		return $instance;
	}

	/**
	 * @param array $instance
	 */
	public function form( $instance ) {
		//Defaults
		$instance = wp_parse_args( (array) $instance, array( 'title' => '') );
		$title = esc_attr( $instance['title'] );
		$count = isset($instance['count']) ? (bool) $instance['count'] :false;
		$hierarchical = isset( $instance['hierarchical'] ) ? (bool) $instance['hierarchical'] : false;
		$dropdown = isset( $instance['dropdown'] ) ? (bool) $instance['dropdown'] : false;
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('dropdown'); ?>" name="<?php echo $this->get_field_name('dropdown'); ?>"<?php checked( $dropdown ); ?> />
		<label for="<?php echo $this->get_field_id('dropdown'); ?>"><?php _e( 'Display as dropdown' ); ?></label><br />

		<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
		<label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show post counts' ); ?></label><br />

		<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('hierarchical'); ?>" name="<?php echo $this->get_field_name('hierarchical'); ?>"<?php checked( $hierarchical ); ?> />
		<label for="<?php echo $this->get_field_id('hierarchical'); ?>"><?php _e( 'Show hierarchy' ); ?></label></p>
<?php
	}

}

if (!function_exists('my_unregister_default_wp_widgets')) {
    function my_unregister_default_wp_widgets() {
        unregister_widget('WP_Widget_Categories');
        register_widget('WP_Widget_Categories_Child');
    }
    add_action('widgets_init', 'my_unregister_default_wp_widgets', 1);
}

// Changing excerpt length
function the_excerpt_post($id, $charlength) {
  $the_post = get_post($id);
  $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
  $excerpt_length = $charlength; //Sets excerpt length by word count
  $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
  $words = explode(' ', $the_excerpt, $excerpt_length + 1);

  if(count($words) > $excerpt_length) :
      array_pop($words);
      array_push($words, '…');
      $the_excerpt = implode(' ', $words);
  endif;

  $the_excerpt = '<p>' . $the_excerpt . '</p>';
  return $the_excerpt;
}


/**
 * Add news custom post type
 */
function register_news_post_type() {
	$labels = array(
		'name'                => 'News',
		'singular_name'       => 'News',
		'menu_name'           => 'News',
		'name_admin_bar'      => 'News',
		'parent_item_colon'   => 'Parent News',
		'all_items'           => 'All News',
		'add_new_item'        => 'Add New News',
		'add_new'             => 'Add New',
		'new_item'            => 'New News',
		'edit_item'           => 'Edit News',
		'update_item'         => 'Update News',
		'view_item'           => 'View News',
		'search_items'        => 'Search News',
		'not_found'           => 'Not found',
		'not_found_in_trash'  => 'Not found in Trash',
	);

	$args = array(
		'label'               => 'News',
		'description'         => 'News',
		'labels'              => $labels,
		'supports'            => array('title', 'editor', 'thumbnail'),
		'taxonomies'          => array( ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-media-text',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'rewrite'             => array('with_front' => false),
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	register_post_type( 'news', $args );
}
add_action( 'init', 'register_news_post_type' );

/**
 * Add talent custom post type
 */
function register_talent_post_type() {
	$labels = array(
		'name'                => 'Talent',
		'singular_name'       => 'Talent',
		'menu_name'           => 'Talent',
		'name_admin_bar'      => 'Talent',
		'parent_item_colon'   => 'Parent Talent',
		'all_items'           => 'All Talent',
		'add_new_item'        => 'Add New Talent',
		'add_new'             => 'Add New',
		'new_item'            => 'New Talent',
		'edit_item'           => 'Edit Talent',
		'update_item'         => 'Update Talent',
		'view_item'           => 'View Talent',
		'search_items'        => 'Search Talent',
		'not_found'           => 'Not found',
		'not_found_in_trash'  => 'Not found in Trash',
	);

	$args = array(
		'label'               => 'Talent',
		'description'         => 'Talent',
		'labels'              => $labels,
		'supports'            => array('title', 'editor'),
		'taxonomies'          => array( ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 6,
		'menu_icon'           => 'dashicons-groups',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'rewrite'             => array('with_front' => false),
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	register_post_type( 'talent', $args );
}
add_action( 'init', 'register_talent_post_type' );

/**
 * Add team custom post type
 */
function register_team_post_type() {
	$labels = array(
		'name'                => 'Team',
		'singular_name'       => 'Team',
		'menu_name'           => 'Team',
		'name_admin_bar'      => 'Team',
		'parent_item_colon'   => 'Parent Team',
		'all_items'           => 'All Team',
		'add_new_item'        => 'Add New Team',
		'add_new'             => 'Add New',
		'new_item'            => 'New Team',
		'edit_item'           => 'Edit Team',
		'update_item'         => 'Update Team',
		'view_item'           => 'View Team',
		'search_items'        => 'Search Team',
		'not_found'           => 'Not found',
		'not_found_in_trash'  => 'Not found in Trash',
	);

	$args = array(
		'label'               => 'Team',
		'description'         => 'Team',
		'labels'              => $labels,
		'supports'            => array('title', 'editor', 'thumbnail'),
		'taxonomies'          => array( ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 6,
		'menu_icon'           => 'dashicons-id-alt',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'rewrite'             => array('with_front' => false),
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	register_post_type( 'team', $args );

	$labels_team = array(
        'name' => __( 'Team Categories', 'eltd_cpt' ),
        'singular_name' => __( 'Team Category', 'eltd_cpt' ),
        'search_items' =>  __( 'Search Team Categories','eltd_cpt' ),
        'all_items' => __( 'All Team Categories','eltd_cpt' ),
        'parent_item' => __( 'Parent Team Category','eltd_cpt' ),
        'parent_item_colon' => __( 'Parent Team Category:','eltd_cpt' ),
        'edit_item' => __( 'Edit Team Category','eltd_cpt' ),
        'update_item' => __( 'Update Team Category','eltd_cpt' ),
        'add_new_item' => __( 'Add New Team Category','eltd_cpt' ),
        'new_item_name' => __( 'New W Category Name','eltd_cpt' ),
        'menu_name' => __( 'Team Categories','eltd_cpt' ),
    );

    register_taxonomy('team_category', array('team'), array(
        'hierarchical' => true,
        'labels' => $labels_team,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'team-category' ),
    ));
}
add_action( 'init', 'register_team_post_type' );

/**
 * Add shortcode for talent page:
 * 			[talent max_post="5"]
 */

function shortcode_tanlent_display( $atts ) {
    $shortcode_attr = shortcode_atts( array(
        'max_post' => '-1',
    ), $atts );

    $args = array(
    	'post_type'=>'talent',
    	'posts_per_page' => $shortcode_attr['max_post'],
    );

    // the query
	$the_query = new WP_Query( $args );
	// var_dump($the_query);die;

	$result = '<p style="text-align: center;">';
	// The Loop
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$result .= '<a href="'.get_permalink($the_query->post->ID).'">'.$the_query->post->post_title.'</a><br>';
		}
	}
	$result .= "</p>";
	ob_start();
	echo $result;
	return ob_get_clean();

}
add_shortcode( 'talent', 'shortcode_tanlent_display' );


//load the css and js for the custom slick slider on about page
function thi_slider_scripts() {
    wp_enqueue_style( 'slick-css', get_stylesheet_directory_uri() . '/css/slick-theme.css' );
    wp_enqueue_style( 'slick-theme-css', get_stylesheet_directory_uri() . '/css/slick.css' );
    wp_enqueue_script( 'slickjs', get_stylesheet_directory_uri() . '/js/slick.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'thi_slider_scripts' );


// show the mailchimp bar only on provoke page
add_filter( 'mctb_show_bar', function( $show ) {
    return is_page('provoke-weekly');
} );

// add rewrite rules for news pagination
function news_add_rewrite_rules() {
  add_rewrite_rule('^news/page/([0-9]+)/?','index.php?page_id=5650&news_page=$matches[1]','top');
}
add_filter('init', 'news_add_rewrite_rules');

function add_query_vars_filter( $vars ){
  $vars[] = "news_page";
  return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );


/**
 * Overloaded theme function to display SEO title instead,
 * also use the blog name as sufix
 */
if(!function_exists('eltd_wp_title_text')) {
	/**
	 * Function that sets page's title. Hooks to wp_title filter
	 * @param $title string current page title
	 * @return string changed title text if SEO plugins aren't installed
	 */
	function eltd_wp_title_text($title) {
		global $eltd_options;
		//get current post id
		$id = eltd_get_page_id();
		$sep = ' | ';
		$title_prefix = '';
		$title_suffix = get_bloginfo('name');

		//is WP 4.1 at least?
		if(function_exists('_wp_render_title_tag')) {
			//set unchanged title variable so we can use it later
			$unchanged_title = $title['title'];
		}

		//pre 4.1 version of WP
		else {
			//set unchanged title variable so we can use it later
			$unchanged_title = $title;
		}

		//is eltd seo enabled?
		if(isset($eltd_options['disable_eltd_seo']) && $eltd_options['disable_eltd_seo'] !== 'yes') {
			//get current post seo title
			$seo_title = esc_attr(get_post_meta($id, "seo_title", true));
			//is current post seo title set?
			if($seo_title !== '') {
				$title_prefix = $seo_title;
			}else{
				$title_prefix =  ucwords( strtolower( $unchanged_title ) );
			}
		}

		//concatenate title string
		$title['title']  = $title_prefix.$sep.$title_suffix;
		unset($title['site']);

		//return generated title string
		return $title;
	}

	add_filter('document_title_parts', 'eltd_wp_title_text', 10, 2);
}

/**
 * decide if must be served as https or http
 */
function thi_correct_url_served_as($string){
	if(is_ssl()){
		$string = str_replace('http://', 'https://', $string);
	}
	return $string;
}
