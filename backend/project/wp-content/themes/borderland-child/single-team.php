<?php

$id = get_the_ID();

?>
<?php get_header(); ?>

        <div class="container"<?php eltd_inline_style($background_color); ?>>
            <?php if($eltd_options['overlapping_content'] == 'yes') {?>
                <div class="overlapping_content"><div class="overlapping_content_inner">
            <?php } ?>
            <div class="container_inner default_template_holder" <?php eltd_inline_style($content_style); ?>>

                <?php if(($sidebar == "default")||($sidebar == "")) : ?>
                    <div class="team_single">
                        <?php
                            get_template_part('templates/blog/blog_team', 'loop');
                        ?>
                        <?php
                            if($blog_single_show_comments == "yes"){
                                comments_template('', true);
                            }else{
                                echo "<br/><br/>";
                            }
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php if($eltd_options['overlapping_content'] == 'yes') {?>
            </div></div>
        <?php } ?>
    </div>

<?php get_footer(); ?>  