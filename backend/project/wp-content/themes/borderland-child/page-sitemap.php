<?php 
/*
Template Name: Sitemap
*/
global $wp_query, $eltd_options;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "eltd_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "eltd_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "eltd_page_background_color", true) != ""){
	$background_color = 'background-color: '.esc_attr(get_post_meta($id, "eltd_page_background_color", true));
}else{
	$background_color = "";
}

$content_style = "";
if(get_post_meta($id, "eltd_content-top-padding", true) != ""){
	if(get_post_meta($id, "eltd_content-top-padding-mobile", true) == 'yes'){
		$content_style = "padding-top:".esc_attr(get_post_meta($id, "eltd_content-top-padding", true))."px !important";
	}else{
		$content_style = "padding-top:".esc_attr(get_post_meta($id, "eltd_content-top-padding", true))."px";
	}
}

$pagination_classes = '';
if( isset($eltd_options['pagination_type']) && $eltd_options['pagination_type'] == 'standard' ) {
	if( isset($eltd_options['pagination_standard_position']) && $eltd_options['pagination_standard_position'] !== '' ) {
		$pagination_classes .= "standard_".esc_attr($eltd_options['pagination_standard_position']);
	}
}
elseif ( isset($eltd_options['pagination_type']) && $eltd_options['pagination_type'] == 'arrows_on_sides' ) {
	$pagination_classes .= "arrows_on_sides";
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
	<?php get_header(); ?>
		<?php if(get_post_meta($id, "eltd_page_scroll_amount_for_sticky", true)) { ?>
			<script>
			var page_scroll_amount_for_sticky = <?php echo esc_attr(get_post_meta($id, "eltd_page_scroll_amount_for_sticky", true)); ?>;
			</script>
		<?php } ?>

		<?php get_template_part( 'title' ); ?>
		<?php get_template_part('slider'); ?>

		<div class="container"<?php eltd_inline_style($background_color); ?>>
        <?php if($eltd_options['overlapping_content'] == 'yes') {?>
            <div class="overlapping_content"><div class="overlapping_content_inner">
        <?php } ?>

                <div class="container_inner default_template_holder clearfix" <?php eltd_inline_style($content_style); ?>>
					<?php get_template_part( 'sitemap' ); ?>
		    	</div>
            <?php if($eltd_options['overlapping_content'] == 'yes') {?>
                </div></div>
            <?php } ?>

  </div>
	    </div>
	<?php get_footer(); ?>
	<script>
	    $j(function() {

	      var teamSlider = function() {
	        $j('.js-slick-slider').slick({
	              rows: 2,
	              slidesToShow: 4,
	              slidesToScroll: 1,
	              dots: true,
	              autoplay: true,
	              //autoplaySpeed: 2000,
	              responsive: [
	                            {
	                              breakpoint: 1000,
	                              settings: {
	                                rows: 2,
	                                slidesToShow: 3,
	                                slidesToScroll: 1,
	                              }
	                            },
	                            {
	                              breakpoint: 600,
	                              settings: {
	                                row: 1,
	                                slidesToShow: 2,
	                                slidesToScroll: 1,
	                              }
	                            },
	                            {
	                              breakpoint: 480,
	                              settings: {
	                                row: 1,
	                                slidesToShow: 1,
	                                slidesToScroll: 1,
	                                dots: false,
	                              }
	                            }
	            ]
	          });
	      }
	      teamSlider();

	    });
	</script>
