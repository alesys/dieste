<?php 

global $wp_query, $eltd_options;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "eltd_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "eltd_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "eltd_page_background_color", true) != ""){
	$background_color = 'background-color: '.esc_attr(get_post_meta($id, "eltd_page_background_color", true));
}else{
	$background_color = "";
}

$content_style = "";
if(get_post_meta($id, "eltd_content-top-padding", true) != ""){
	if(get_post_meta($id, "eltd_content-top-padding-mobile", true) == 'yes'){
		$content_style = "padding-top:".esc_attr(get_post_meta($id, "eltd_content-top-padding", true))."px !important";
	}else{
		$content_style = "padding-top:".esc_attr(get_post_meta($id, "eltd_content-top-padding", true))."px";
	}
}

$pagination_classes = '';
if( isset($eltd_options['pagination_type']) && $eltd_options['pagination_type'] == 'standard' ) {
	if( isset($eltd_options['pagination_standard_position']) && $eltd_options['pagination_standard_position'] !== '' ) {
		$pagination_classes .= "standard_".esc_attr($eltd_options['pagination_standard_position']);
	}
}
elseif ( isset($eltd_options['pagination_type']) && $eltd_options['pagination_type'] == 'arrows_on_sides' ) {
	$pagination_classes .= "arrows_on_sides";
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
	<?php get_header(); ?>
		<?php if(get_post_meta($id, "eltd_page_scroll_amount_for_sticky", true)) { ?>
			<script>
			var page_scroll_amount_for_sticky = <?php echo esc_attr(get_post_meta($id, "eltd_page_scroll_amount_for_sticky", true)); ?>;
			</script>
		<?php } ?>

		<?php get_template_part( 'title' ); ?>
		<?php get_template_part('slider'); ?>

		<div class="container"<?php eltd_inline_style($background_color); ?>>
        <?php if($eltd_options['overlapping_content'] == 'yes') {?>
            <div class="overlapping_content"><div class="overlapping_content_inner">
        <?php } ?>

                <div class="container_inner default_template_holder clearfix" <?php eltd_inline_style($content_style); ?>>
				<?php if(($sidebar == "default")||($sidebar == "")) : ?>
					<?php if (have_posts()) : 
							while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
							<?php 
								$args_pages = array(
									'before'           => '<div class="single_links_pages ' .$pagination_classes. '"><div class="single_links_pages_inner">',
									'after'            => '</div></div>',
									'pagelink'         => '<span>%</span>'
								);
								wp_link_pages($args_pages);
							?>
							<?php
							if($enable_page_comments){
								comments_template('', true); 
							}
							?> 
							<?php endwhile; ?>
						<?php endif; ?>
				<?php elseif($sidebar == "1" || $sidebar == "2"): ?>		
					
					<?php if($sidebar == "1") : ?>	
						<div class="two_columns_66_33 background_color_sidebar grid2 clearfix">
							<div class="column1 content_left_from_sidebar">
					<?php elseif($sidebar == "2") : ?>	
						<div class="two_columns_75_25 background_color_sidebar grid2 clearfix">
							<div class="column1 content_left_from_sidebar">
					<?php endif; ?>
							<?php if (have_posts()) : 
								while (have_posts()) : the_post(); ?>
								<div class="column_inner">
								
								<?php the_content(); ?>
								<?php 
									$args_pages = array(
									'before'           => '<div class="single_links_pages ' .$pagination_classes. '"><div class="single_links_pages_inner">',
									'after'            => '</div></div>',
									'pagelink'         => '<span>%</span>'
									);

									wp_link_pages($args_pages);
								?>
								<?php
								if($enable_page_comments){
									comments_template('', true); 
								}
								?> 
								</div>
						<?php endwhile; ?>
						<?php endif; ?>
					
									
							</div>
							<div class="column2"><?php get_sidebar();?></div>
						</div>
					<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
						<?php if($sidebar == "3") : ?>	
							<div class="two_columns_33_66 background_color_sidebar grid2 clearfix">
								<div class="column1"><?php get_sidebar();?></div>
								<div class="column2 content_right_from_sidebar">
						<?php elseif($sidebar == "4") : ?>	
							<div class="two_columns_25_75 background_color_sidebar grid2 clearfix">
								<div class="column1"><?php get_sidebar();?></div>
								<div class="column2 content_right_from_sidebar">
						<?php endif; ?>
								<?php if (have_posts()) : 
									while (have_posts()) : the_post(); ?>
									<div class="column_inner">
										<?php the_content(); ?>
										<?php 
											$args_pages = array(
												'before'           => '<div class="single_links_pages ' .$pagination_classes. '"><div class="single_links_pages_inner">',
												'after'            => '</div></div>',
												'pagelink'         => '<span>%</span>'
											);
											wp_link_pages($args_pages);
										?>
										<?php
										if($enable_page_comments){
											comments_template('', true); 
										}
										?> 
									</div>
							<?php endwhile; ?>
							<?php endif; ?>
						
										
								</div>
								
							</div>
					<?php endif; ?>
		    </div>
            <?php if($eltd_options['overlapping_content'] == 'yes') {?>
                </div></div>
            <?php } ?>

            <?php
            	//add second slider
            	$args = array( 
            		'post_type' => 'team', 
            		'posts_per_page' => -1,
            		'tax_query' => array(
						array(
							'taxonomy' => 'team_category',
							'field'    => 'slug',
							'terms'    => 'team',
						),
					),
            	);
            	$the_team_query = new WP_Query( $args );
            	if ( $the_team_query->have_posts() ){
            ?>
		            <div class="container-team-slider">
					    <div class="slider-label"><div class="text-holder"><div class="text-inner"><p>Team</p></div></div></div>
					    <div class="js-slick-slider">
					    	<?php while ( $the_team_query->have_posts() ){
					    		$the_team_query->the_post(); 
					    		$terms = wp_get_post_terms($the_team_query->post->ID, 'team_category');
								$category_html = '';
								$k = 1;
					    		foreach ($terms as $term) {
				                    $category_html .= "$term->name";
				                    if (count($terms) != $k) {
				                        $category_html .= ' / ';
				                    }
				                    $k++;
				                }
					    	?>
						      	<div class="sp-col">
						        	<div class="text-holder"><div class="text-holder-tb"><div class="text-holder-inner"><h5><?php echo $the_team_query->post->post_title ?></h5><p><span> </span><?php echo $category_html; ?></p></div></div></div>
						        	<?php 
						        		$slider_thumb_id = get_post_thumbnail_id($the_team_query->post->ID);
										$slider_thumb_url = wp_get_attachment_image_src($slider_thumb_id,'portfolio-landscape', true);
						        	?>
						        	<div class="img-holder" style="background-image: url(<?php echo $slider_thumb_url[0]; ?>);"></div>
						      	</div>
						    <?php } ?>
					    </div>
					</div>
            <?php 
            	}
            ?>
  </div>
	    </div>
	<?php get_footer(); ?>
	<script>
	    $j(function() {

	      var teamSlider = function() {
	        $j('.js-slick-slider').slick({
	              rows: 2,
	              slidesToShow: 4,
	              slidesToScroll: 1,
	              dots: true,
	              autoplay: true,
	              //autoplaySpeed: 2000,
	              responsive: [
	                            {
	                              breakpoint: 1000,
	                              settings: {
	                                rows: 2,
	                                slidesToShow: 3,
	                                slidesToScroll: 1,
	                              }
	                            },
	                            {
	                              breakpoint: 600,
	                              settings: {
	                                row: 1,
	                                slidesToShow: 2,
	                                slidesToScroll: 1,
	                              }
	                            },
	                            {
	                              breakpoint: 480,
	                              settings: {
	                                row: 1,
	                                slidesToShow: 1,
	                                slidesToScroll: 1,
	                                dots: false,
	                              }
	                            }
	            ]
	          });
	      }
	      teamSlider();

	    });
	</script>
