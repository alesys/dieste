<?php
global $eltd_options;

$blog_show_comments = "yes";
if (isset($eltd_options['blog_single_show_comments'])) {
    $blog_show_comments = $eltd_options['blog_single_show_comments'];
}
$blog_show_author = "yes";
if (isset($eltd_options['blog_author_info'])) {
    $blog_show_author = $eltd_options['blog_author_info'];
}
$blog_show_like = "yes";
if (isset($eltd_options['blog_single_show_like'])) {
    $blog_show_like = $eltd_options['blog_single_show_like'];
}
$blog_show_date = "yes";
if (isset($eltd_options['blog_single_show_date'])) {
    $blog_show_date = $eltd_options['blog_single_show_date'];
}

$blog_social_share_type = "dropdown";
if(isset($eltd_options['blog_single_select_share_option'])){
	$blog_social_share_type = $eltd_options['blog_single_select_share_option'];
}
$blog_show_social_share = "no";
if (isset($eltd_options['enable_social_share'])&& ($eltd_options['enable_social_share']) =="yes"){
    if (isset($eltd_options['post_types_names_post'])&& $eltd_options['post_types_names_post'] =="post"){
        if (isset($eltd_options['blog_single_show_social_share'])) {
            $blog_show_social_share = $eltd_options['blog_single_show_social_share'];
        }
    }
}
$blog_show_categories = "yes";
if (isset($eltd_options['blog_single_show_category'])) {
    $blog_show_categories = $eltd_options['blog_single_show_category'];
}
$blog_show_ql_icon = "yes";
$blog_title_holder_icon_class = "";
if (isset($eltd_options['blog_single_show_ql_icon'])) {
    $blog_show_ql_icon = $eltd_options['blog_single_show_ql_icon'];
}

if($blog_show_ql_icon == "yes"){
	$blog_title_holder_icon_class = " with_icon_right";
}

$blog_ql_background_image = "no";
if(isset($eltd_options['blog_standard_type_ql_background_image'])){
	$blog_ql_background_image = $eltd_options['blog_standard_type_ql_background_image'];
}
$background_image_object = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID()), 'blog_image_format_link_quote');
$background_image_src = $background_image_object[0];

$_post_format = get_post_format();

$background_image_html = '';
$background_image_html_class = '';
if($blog_ql_background_image == "yes"){
	if(get_post_meta(get_the_ID(), "eltd_hide-featured-image", true) != "yes"){
		if($_post_format == "quote"){
			$background_image_html_class .=  ' quote_image';
		}elseif($_post_format == "link"){
			$background_image_html_class .=  ' link_image';
		}
		$background_image_html = 'background-image: url(' . esc_url($background_image_src) . ')';
	}
}

$upload_dir = wp_upload_dir();
?>
<?php
	// var_dump($_post_format);die;
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="post_content_holder">
            <?php if(get_post_meta(get_the_ID(), "eltd_hide-featured-image", true) != "yes") {
                if ( has_post_thumbnail() ) { ?>
                    <div class="post_image">
                        <?php the_post_thumbnail('full'); ?>
                    </div>
			<?php }} ?>



			<div class="post_text talent-descript-pg">
				<div class="post_text_inner">
                    <div class="inner_content_blog_text">
                      	
          					<div class="full_section_inner clearfix" >
          						<div class="section header">
          							
          							<h3 style="text-align: center;"><?php the_title(); ?></h3>
          						</div>
          					</div>

						<?php
                            the_content();
						?>
							<div class="full_section_inner read_more_wrapper read_more_wrapper_talent sp-btns-wrapper clearfix" >
          				<div class="section btn-bottom-row blog_social_share_wrapper">
          							<!-- <a class="left-arrow" href="<?php echo get_post_permalink(5656, true); ?>"><img src="<?php echo $upload_dir['baseurl'];?>/2016/03/left_arrow_03.png" alt="arrow" /> BACK TO CAREERS</a> -->
                        <a href="<?php echo get_post_permalink(5656, true); ?>" target="_self" class="back_btn qbutton small read_more_button "><img src="<?php echo $upload_dir['baseurl']; ?>/2016/03/left_arrow_03.png" alt="arrow"> Back</a>
                        <div class="blog_share">
                          <?php echo do_shortcode('[no_social_share]'); ?>
                        </div>
          				</div>
          		</div>
					</div>
      			</div>
			</div>
		</div>
	</article>
