<?php
global $eltd_options;

$blog_show_comments = "yes";
if (isset($eltd_options['blog_single_show_comments'])) {
    $blog_show_comments = $eltd_options['blog_single_show_comments'];
}
$blog_show_author = "yes";
if (isset($eltd_options['blog_author_info'])) {
    $blog_show_author = $eltd_options['blog_author_info'];
}
$blog_show_like = "yes";
if (isset($eltd_options['blog_single_show_like'])) {
    $blog_show_like = $eltd_options['blog_single_show_like'];
}
$blog_show_date = "yes";
if (isset($eltd_options['blog_single_show_date'])) {
    $blog_show_date = $eltd_options['blog_single_show_date'];
}

$blog_social_share_type = "dropdown";
if(isset($eltd_options['blog_single_select_share_option'])){
	$blog_social_share_type = $eltd_options['blog_single_select_share_option'];
}
$blog_show_social_share = "no";
if (isset($eltd_options['enable_social_share'])&& ($eltd_options['enable_social_share']) =="yes"){
    if (isset($eltd_options['post_types_names_post'])&& $eltd_options['post_types_names_post'] =="post"){
        if (isset($eltd_options['blog_single_show_social_share'])) {
            $blog_show_social_share = $eltd_options['blog_single_show_social_share'];
        }
    }
}
$blog_show_categories = "yes";
if (isset($eltd_options['blog_single_show_category'])) {
    $blog_show_categories = $eltd_options['blog_single_show_category'];
}
$blog_show_ql_icon = "yes";
$blog_title_holder_icon_class = "";
if (isset($eltd_options['blog_single_show_ql_icon'])) {
    $blog_show_ql_icon = $eltd_options['blog_single_show_ql_icon'];
}

if($blog_show_ql_icon == "yes"){
	$blog_title_holder_icon_class = " with_icon_right";
}

$blog_ql_background_image = "no";
if(isset($eltd_options['blog_standard_type_ql_background_image'])){
	$blog_ql_background_image = $eltd_options['blog_standard_type_ql_background_image'];
}
$background_image_object = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID()), 'blog_image_format_link_quote');
$background_image_src = $background_image_object[0];

$_post_format = get_post_format();

$background_image_html = '';
$background_image_html_class = '';
if($blog_ql_background_image == "yes"){
	if(get_post_meta(get_the_ID(), "eltd_hide-featured-image", true) != "yes"){
		if($_post_format == "quote"){
			$background_image_html_class .=  ' quote_image';
		}elseif($_post_format == "link"){
			$background_image_html_class .=  ' link_image';
		}
		$background_image_html = 'background-image: url(' . esc_url($background_image_src) . ')';
	}
}
?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="post_content_holder">
				<?php 
					$team_categories = get_the_terms($post, 'team_category');
					$team_cats = array();
					foreach ($team_categories as $cat) {
						$team_cats[] = $cat->name;
					}
					$cats_string = implode(', ', $team_cats);
				?>
				<h3 class="tema_title" ><?php the_title(); ?> <strong><?php echo $cats_string; ?></strong></h3>
                <?php 
                	$detail_image = (string) get_custom_field('detail_image');
                    if ( $detail_image != '' ){
                ?>
                        <div class="post_image team_image" style="background: url(<?php echo $detail_image; ?>)"></div>
				<?php }else{
							$thumb_id = get_post_thumbnail_id();
							$thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);
				?>
						<div class="post_image team_image" style="background: url(<?php echo $thumb_url[0]; ?>)"></div>
				<?php 
					} 
				?>

				<div class="post_text">
					<div class="post_text_inner">
                        <div class="inner_content_blog_text">
                          <?php if($blog_show_author == "yes" || $blog_show_date == "yes" || $blog_show_categories == "yes" || $blog_show_comments == "yes" || $blog_show_like == "yes") { ?>
              					<?php if($post->post_type == 'post'){ ?>
              							<div class="post_info single-blog-info">
              								<?php eltd_post_info(array('date' => $blog_show_date, 'author' => $blog_show_author, 'category' => $blog_show_categories, 'comments' => $blog_show_comments, 'like' => $blog_show_like));?>
              							</div>
              					<?php } ?>
                            <?php if( has_category()) { ?>
                                <div class="single_tags">
                                    <div class="tags_text">
                                        <<?php echo esc_attr($title_tag);?> class="single_tags_heading"><?php _e('', 'eltd'); ?></<?php echo esc_attr($title_tag);?>>
                                        <?php $categories = wp_get_post_categories(get_the_ID());
                                              $html = '<div class="post_tags">';
                                              foreach ( $categories as $category_id ) {
                                                $category = get_category( $category_id );
                                              	$tag_link = get_tag_link( $category->term_id );
                                                $tag_class = $category->slug . '-tag-name tags-post';
                                              	$html .= "<a href='{$tag_link}' title='{$category->name} Tag' class='{$tag_class}'>";
                                              	$html .= "<span class='dot-tags'> • </span> {$category->name}</a>";
                                              }
                                              $html .= '</div>';
                                              echo $html; ?>
                                    </div>
                                </div>
                            <?php } ?>
              						<?php } ?>
							<?php
                                the_content();
							?>

							<?php if ($blog_show_social_share == "yes"){ ?>
								<div class="read_more_wrapper sp-btns-wrapper clearfix">
									<?php
										if($blog_social_share_type == "dropdown"){
											eltd_post_info(array('share' => $blog_show_social_share));
										}
									?>
								</div>
							<?php } ?>
							<?php if(isset($eltd_options['blog_single_show_social_share']) && $eltd_options['blog_single_show_social_share'] == "yes" && $blog_social_share_type == "list") {
								echo do_shortcode('[no_social_share_list]'); // XSS OK
							}; ?>

							<?php if($post->post_type == 'post'){ ?>
									<div class="related_articles">
										<div class="text-align-center">
											<h5>Related Articles</h5>
										</div>
										<?php wp_related_posts(); ?>
									</div>
							<?php } ?>

						</div>
          </div>
				</div>
			</div>
		</article>
