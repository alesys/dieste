<div class="vc_row wpb_row section">
    <div class=" full_section_inner clearfix">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="wpb_wrapper">
                <div class="vc_row wpb_row vc_inner section about-info-sec provoke-info ">
                    <h4 id="pages">Pages</h4>
                    <ul>
                        <?php
                            // Add pages you'd like to exclude in the exclude here
                            wp_list_pages(
                                array(
                                    'exclude' => '',
                                    'title_li' => '',
                                )
                            );
                        ?>
                    </ul>

                    <h4 id="posts">Posts</h4>
                    <ul>
                        <?php
                            // Add categories you'd like to exclude in the exclude here
                            $cats = get_categories('exclude=');
                            foreach ($cats as $cat) {
                                echo "<li><h3>".$cat->cat_name."</h3>";
                                echo "<ul>";
                                query_posts('posts_per_page=-1&cat='.$cat->cat_ID);
                                while(have_posts()) {
                                    the_post();
                                    $category = get_the_category();
                                    // Only display a post link once, even if it's in multiple categories
                                    if ($category[0]->cat_ID == $cat->cat_ID) {
                                        echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
                                    }
                                }
                                echo "</ul>";
                                echo "</li>";
                            }
                        ?>
                    </ul>
                    <?php 
                    foreach( get_post_types( array('public' => true) ) as $post_type ) {
                      if ( in_array( $post_type, array('post','page','attachment', 'internship-sections', 'people-brands') ) )
                        continue;

                      $pt = get_post_type_object( $post_type );

                      echo '<h4>'.$pt->labels->name.'</h4>';
                      echo '<ul>';

                      query_posts('post_type='.$post_type.'&posts_per_page=-1');
                      while( have_posts() ) {
                        the_post();
                        echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
                      }

                      echo '</ul>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>