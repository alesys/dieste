<?php
global $eltd_options, $eltdIconCollections;

/* Set id on -1 beacause archive page id can have same id as some post and settings is not good */
if(is_category() || is_tag() || is_author()){
	$archive_id = $id;
	$id = eltd_get_page_id();
}

if(get_post_meta($id, "eltd_responsive-title-image", true) != ""){
 $responsive_title_image = get_post_meta($id, "eltd_responsive-title-image", true);
}else{
	$responsive_title_image = $eltd_options['responsive_title_image'];
}

if(get_post_meta($id, "eltd_fixed-title-image", true) != ""){
 $fixed_title_image = get_post_meta($id, "eltd_fixed-title-image", true);
}else{
	$fixed_title_image = $eltd_options['fixed_title_image'];
}

$title_image = '';
if(get_post_meta($id, "eltd_title-image", true) != ""){
    $title_image = get_post_meta($id, "eltd_title-image", true);
}else{
    $title_image = $eltd_options['title_image'];
}
$title_image_height = "";
$title_image_width = "";
if($title_image !== ''){
	$title_image_dimensions_array = eltd_get_image_dimensions($title_image);
	if (count($title_image_dimensions_array)) {
	    $title_image_height = $title_image_dimensions_array["height"];
	    $title_image_width = $title_image_dimensions_array["width"];
	}
}

$title_graphics = "";
if (get_post_meta($id, "eltd_title-graphics", true) !== "") {
    $title_graphics = get_post_meta($id, "eltd_title-graphics", true);
}
elseif($eltd_options['title_graphics'] != "") {
    $title_graphics = $eltd_options['title_graphics'];
}

//Whole Title Content Animation
$title_content_animation = 'no';
if (get_post_meta($id, 'page_page_title_whole_content_animations', true) !== '') {
    $title_content_animation = get_post_meta($id, 'page_page_title_whole_content_animations', true);
}
elseif (get_post_meta($id, 'page_page_title_whole_content_animations', true) == '' && isset($eltd_options['page_title_whole_content_animations']) && $eltd_options['page_title_whole_content_animations'] !== '') {
    $title_content_animation = $eltd_options['page_title_whole_content_animations'];
}

$page_title_content_animation_data = '';
if ($title_content_animation == 'yes') {

    $page_title_content_data_start = '0';
    $page_title_content_start_custom_style = 'opacity:1';
    $page_title_content_data_end = '300';
    $page_title_content_end_custom_style = 'opacity:0';

    if (get_post_meta($id, 'page_page_title_whole_content_data_start', true) == '' && isset($eltd_options['page_title_whole_content_data_start']) && $eltd_options['page_title_whole_content_data_start'] !== '') {
        $page_title_content_data_start = $eltd_options['page_title_whole_content_data_start'];
    } elseif (get_post_meta($id, 'page_page_title_whole_content_data_start', true) !== '') {
        $page_title_content_data_start = get_post_meta($id, 'page_page_title_whole_content_data_start', true);
    }

    if (get_post_meta($id, 'page_page_title_whole_content_start_custom_style', true) == '' && isset($eltd_options['page_title_whole_content_start_custom_style']) && $eltd_options['page_title_whole_content_start_custom_style'] !== '') {
        $page_title_content_start_custom_style = $eltd_options['page_title_whole_content_start_custom_style'];
    } elseif (get_post_meta($id, 'page_page_title_whole_content_start_custom_style', true) !== '') {
        $page_title_content_start_custom_style = get_post_meta($id, 'page_page_title_whole_content_start_custom_style', true);
    }

    if (get_post_meta($id, 'page_page_title_whole_content_data_end', true) == '' && isset($eltd_options['page_title_whole_content_data_end']) && $eltd_options['page_title_whole_content_data_end'] !== '') {
        $page_title_content_data_end = $eltd_options['page_title_whole_content_data_end'];
    } elseif (get_post_meta($id, 'page_page_title_whole_content_data_end', true) !== '') {
        $page_title_content_data_end = get_post_meta($id, 'page_page_title_whole_content_data_end', true);
    }

    if (get_post_meta($id, 'page_page_title_whole_content_end_custom_style', true) == '' && isset($eltd_options['page_title_whole_content_end_custom_style']) && $eltd_options['page_title_whole_content_end_custom_style'] !== '') {
        $page_title_content_end_custom_style = $eltd_options['page_title_whole_content_end_custom_style'];
    } elseif (get_post_meta($id, 'page_page_title_whole_content_end_custom_style', true) !== '') {
        $page_title_content_end_custom_style = get_post_meta($id, 'page_page_title_whole_content_end_custom_style', true);
    }

    $page_title_content_animation_data = ' data-'.$page_title_content_data_start.'="'.$page_title_content_start_custom_style.'" data-'.$page_title_content_data_end.'="'.$page_title_content_end_custom_style.'"';
}

//Graphic Animation
$graphic_animation = 'no';
if (get_post_meta($id, 'page_page_title_graphic_animations', true) !== '') {
    $graphic_animation = get_post_meta($id, 'page_page_title_graphic_animations', true);
}
elseif (get_post_meta($id, 'page_page_title_graphic_animations', true) == '' && isset($eltd_options['page_title_graphic_animations']) && $eltd_options['page_title_graphic_animations'] !== '') {
    $graphic_animation = $eltd_options['page_title_graphic_animations'];
}

$page_title_graphic_animation_data = '';
if ($title_graphics !== '' && $graphic_animation == 'yes'){

    $page_title_graphic_data_start = '0';
    $page_title_graphic_start_custom_style = 'opacity:1';
    $page_title_graphic_data_end = '300';
    $page_title_graphic_end_custom_style = 'opacity:0';

    if (get_post_meta($id, 'page_page_title_graphic_data_start', true) == '' && isset($eltd_options['page_title_graphic_data_start']) && $eltd_options['page_title_graphic_data_start'] !== '') {
        $page_title_graphic_data_start = $eltd_options['page_title_graphic_data_start'];
    } elseif (get_post_meta($id, 'page_page_title_graphic_data_start', true) !== '') {
        $page_title_graphic_data_start = get_post_meta($id, 'page_page_title_graphic_data_start', true);
    }

    if(get_post_meta($id, 'page_page_title_graphic_start_custom_style', true) == '' && isset($eltd_options['page_title_graphic_start_custom_style']) && $eltd_options['page_title_graphic_start_custom_style'] !== '') {
        $page_title_graphic_start_custom_style = $eltd_options['page_title_graphic_start_custom_style'];
    } elseif (get_post_meta($id, 'page_page_title_graphic_start_custom_style', true) !== '') {
        $page_title_graphic_start_custom_style = get_post_meta($id, 'page_page_title_graphic_start_custom_style', true);
    }

    if(get_post_meta($id, 'page_page_title_graphic_data_end', true) == '' && isset($eltd_options['page_title_graphic_data_end']) && $eltd_options['page_title_graphic_data_end'] !== '') {
        $page_title_graphic_data_end = $eltd_options['page_title_graphic_data_end'];
    } elseif(get_post_meta($id, 'page_page_title_graphic_data_end', true) !== '') {
        $page_title_graphic_data_end = get_post_meta($id, 'page_page_title_graphic_data_end', true);
    }

    if(get_post_meta($id, 'page_page_title_graphic_end_custom_style', true) == '' && isset($eltd_options['page_title_graphic_end_custom_style']) && $eltd_options['page_title_graphic_end_custom_style'] !== '') {
        $page_title_graphic_end_custom_style = $eltd_options['page_title_graphic_end_custom_style'];
    } elseif(get_post_meta($id, 'page_page_title_graphic_end_custom_style', true) !== '') {
        $page_title_graphic_end_custom_style = get_post_meta($id, 'page_page_title_graphic_end_custom_style', true);
    }

    $page_title_graphic_animation_data = ' data-'.$page_title_graphic_data_start.'="'.$page_title_graphic_start_custom_style.'" data-'.$page_title_graphic_data_end.'="'.$page_title_graphic_end_custom_style.'"';

}


if(get_post_meta($id, "eltd_title-overlay-image", true) != ""){
 $title_overlay_image = get_post_meta($id, "eltd_title-overlay-image", true);
}else{
	$title_overlay_image = $eltd_options['title_overlay_image'];
}
$logo_width = $eltd_options['logo_width'];
$logo_height = $eltd_options['logo_height'];
if (isset($eltd_options['header_bottom_appearance'])) {
    $header_bottom_appearance = $eltd_options['header_bottom_appearance'];
}

$header_top_border=0;
$header_bottom_border=0;
if(isset($eltd_options['enable_header_top_border']) && $eltd_options['enable_header_top_border']=='yes' && isset($eltd_options['header_top_border_width']) && $eltd_options['header_top_border_width']!==''){
    $header_top_border = esc_attr($eltd_options['header_top_border_width']);
}
if(isset($eltd_options['enable_header_bottom_border']) && $eltd_options['enable_header_bottom_border']=='yes' && isset($eltd_options['header_bottom_border_width']) && $eltd_options['header_bottom_border_width']!==''){
    $header_bottom_border = esc_attr($eltd_options['header_bottom_border_width']);
}

$header_centered_logo_border = 0;
if(isset($eltd_options['center_logo_image']) && $eltd_options['center_logo_image'] == "yes" && isset($eltd_options['enable_border_top_bottom_menu']) && $eltd_options['enable_border_top_bottom_menu'] == "yes"){
    $header_centered_logo_border = 2;
}

$header_height = 129;
if (!empty($eltd_options['header_height']) && $header_bottom_appearance != "fixed_hiding") {
    $header_height = esc_attr($eltd_options['header_height']);
} elseif($header_bottom_appearance == "fixed_hiding"){
    $header_height = 129 + $logo_height/2 + 40; // 40 is top and bottom margin of logo
} elseif(isset($eltd_options['center_logo_image']) && $eltd_options['center_logo_image'] == "yes" && $header_bottom_appearance == "fixed") {
    if($header_bottom_appearance == "fixed" && $logo_height > 129){
        $header_height = 129 + 129 + 20; //20 is top and bottom margin of logo, 129 is default header height, other 129 is logo height
    }
    if($header_bottom_appearance == "fixed" && $logo_height < 129){
        $header_height = 129 + $logo_height + 20; //20 is top and bottom margin of logo, 129 is default header height
    }
}

if($eltd_options['header_bottom_appearance'] == 'stick menu_bottom'){
    $menu_bottom = 60;
    if(is_active_sidebar('header_fixed_right')){
        $menu_bottom = $menu_bottom + 26;
    }
} else {
    $menu_bottom = 0;
}


$header_height = $header_height + $menu_bottom + $header_top_border + $header_bottom_border + $header_centered_logo_border;

$header_top = 0;
if(isset($eltd_options['header_top_area']) && $eltd_options['header_top_area'] == "yes"){
	if(isset($eltd_options['header_top_height']) && $eltd_options['header_top_height'] !== ""){
		$header_top = $eltd_options['header_top_height'];
	} else {
		$header_top = 36;
	}
}


//title vertical alignment
$title_vertical_alignment = 'header_bottom';
if(get_post_meta($id, "eltd_page_page_title_vertical_aligment", true) == 'header_bottom') {
	$title_vertical_alignment = 'header_bottom';
}elseif(get_post_meta($id, "eltd_page_page_title_vertical_aligment", true) == 'window_top') {
	$title_vertical_alignment = 'window_top';
}elseif(get_post_meta($id, "eltd_page_page_title_vertical_aligment", true) == '' && (isset($eltd_options['page_title_vertical_aligment']) && $eltd_options['page_title_vertical_aligment'] == 'header_bottom')) {
	$title_vertical_alignment = 'header_bottom';
} elseif(get_post_meta($id, "eltd_page_page_title_vertical_aligment", true) == '' && (isset($eltd_options['page_title_vertical_aligment']) && $eltd_options['page_title_vertical_aligment'] == 'window_top')) {
	$title_vertical_alignment = 'window_top';
}


$header_height_padding = 0;

if($title_vertical_alignment=='header_bottom'){

if ($header_bottom_appearance != "fixed" && $header_bottom_appearance != "fixed_hiding" && $header_bottom_appearance != "regular") {
    if ($eltd_options['center_logo_image'] != "yes") {

        $header_height_padding = $header_top + $header_height;

    } else {
        if($header_bottom_appearance == "stick menu_bottom") {

            $header_height_padding = 20 + $logo_height/2 + $menu_bottom + $header_top + $header_top_border + $header_bottom_border + $header_centered_logo_border; // 20 is top margin of centered logo

        } elseif($header_bottom_appearance == "stick_with_left_right_menu"){

            $header_height_padding = $header_height + $header_top;

        }  else {

            $header_height_padding = (20 + $logo_height/2 + $header_height + $header_top); // 20 is top margin of centered logo
        }
    }
} else {
    $header_height_padding = $header_height + $header_top;
}

if (!empty($eltd_options['header_height'])) {
    if($header_bottom_appearance == "fixed_hiding") {
        $header_height_padding =  esc_attr($eltd_options['header_height']) + $header_top + $logo_height/2 + 40 + $header_top_border + $header_bottom_border + $header_centered_logo_border; // 40 is top and bottom margin of logo
    } elseif($eltd_options['center_logo_image'] == "yes"){
        if($header_bottom_appearance == "fixed") {
            if ($eltd_options['header_height'] > $logo_height) {
                $header_height_padding = esc_attr($eltd_options['header_height']) + $header_top + $logo_height + 20 + $header_top_border + $header_bottom_border + $header_centered_logo_border; // 20 is top margin of logo
            } else {
                $header_height_padding = (esc_attr($eltd_options['header_height'])) * 2 + $header_top + 20 + $header_top_border + $header_bottom_border + $header_centered_logo_border; // 20 is top margin of logo
            }
        }
        if($header_bottom_appearance == "stick"){
            $header_height_padding = (20 + $logo_height/2 + esc_attr($eltd_options['header_height']) + $header_top + $header_top_border + $header_bottom_border + $header_centered_logo_border); // 20 is top margin of centered logo
        }
    } else {
        if($header_bottom_appearance != "stick menu_bottom") {
            $header_height_padding = esc_attr($eltd_options['header_height']) + $header_top + $header_top_border + $header_bottom_border + $header_centered_logo_border;
        }
    }
}

}

else if ($title_vertical_alignment=='window_top'){
	$header_height_padding = 0;
}

$title_type = "standard_title";
if(get_post_meta($id, "eltd_page_title_type", true) != ""){
    $title_type = get_post_meta($id, "eltd_page_title_type", true);
}else{
    if(isset($eltd_options['title_type'])){
        $title_type = $eltd_options['title_type'];
    }
}



$subtitle_position = "below_title";
if(get_post_meta($id, "eltd_page_subtitle_position", true) != ""){
    $subtitle_position = get_post_meta($id, "eltd_page_subtitle_position", true);
}else{
    if(isset($eltd_options['subtitle_position'])){
        $subtitle_position = $eltd_options['subtitle_position'];
    }
}

//init variables
$title_subtitle_padding 	= '';
$header_transparency 		= '';
$is_header_transparent  	= false;
$transparent_values_array 	= array('0.00', '0');
$solid_values_array			= array('', '1');
$header_bottom_border		= '';
$is_title_area_visible		= true;
$is_title_text_visible		= true;

//this is done this way because content was already created
//and we had to keep current settings for existing pages
//checkbox that was previously here had 'yes' value when title area is hidden
if(get_post_meta($id, "eltd_show-page-title", true) == 'yes') {
	$is_title_area_visible = true;
} elseif(get_post_meta($id, "eltd_show-page-title", true) == 'no') {
	$is_title_area_visible = false;
} elseif(get_post_meta($id, "eltd_show-page-title", true) == '' && (isset($eltd_options['show_page_title']) && $eltd_options['show_page_title'] == 'yes')) {
	$is_title_area_visible = true;
} elseif(get_post_meta($id, "eltd_show-page-title", true) == '' && (isset($eltd_options['show_page_title']) && $eltd_options['show_page_title'] == 'no')) {
	$is_title_area_visible = false;
} elseif(isset($eltd_options['show_page_title']) && $eltd_options['show_page_title'] == 'yes') {
	$is_title_area_visible = true;
}

//is title text visible
if(get_post_meta($id, "eltd_show_page_title_text", true) == 'yes') {
	$is_title_text_visible = true;
} elseif(get_post_meta($id, "eltd_show_page_title_text", true) == 'no') {
	$is_title_text_visible = false;
} elseif(get_post_meta($id, "eltd_show_page_title_text", true) == '' && (isset($eltd_options['show_page_title_text']) && $eltd_options['show_page_title_text'] == 'yes')) {
	$is_title_text_visible = true;
} elseif(get_post_meta($id, "eltd_show_page_title_text", true) == '' && (isset($eltd_options['show_page_title_text']) && $eltd_options['show_page_title_text'] == 'no')) {
	$is_title_text_visible = false;
} elseif(isset($eltd_options['show_page_title_text']) && $eltd_options['show_page_title_text'] == 'yes') {
	$is_title_text_visible = true;
}

//Skrollr animation for title
$title_animation = 'no';
if (get_post_meta($id, 'page_page_title_animations', true) !== '') {
    $title_animation = get_post_meta($id, 'page_page_title_animations', true);
}
elseif (get_post_meta($id, 'page_page_title_animations', true) == '' && isset($eltd_options['page_title_animations']) && $eltd_options['page_title_animations'] !== '') {
    $title_animation = $eltd_options['page_title_animations'];
}

$page_title_animation_data = "";
if($is_title_text_visible && $title_animation == 'yes') {

    $page_title_data_start = '0';
    $page_title_start_custom_style = 'opacity:1';
    $page_title_data_end = '300';
    $page_title_end_custom_style = 'opacity:0';

    if(get_post_meta($id, 'page_page_title_data_start', true) == '' && isset($eltd_options['page_title_data_start']) && $eltd_options['page_title_data_start'] !== '') {
        $page_title_data_start = $eltd_options['page_title_data_start'];
    } elseif(get_post_meta($id, 'page_page_title_data_start', true) !== '') {
        $page_title_data_start = get_post_meta($id, 'page_page_title_data_start', true);
    }

    if(get_post_meta($id, 'page_page_title_start_custom_style', true) == '' && isset($eltd_options['page_title_start_custom_style']) && $eltd_options['page_title_start_custom_style'] !== '') {
        $page_title_start_custom_style = $eltd_options['page_title_start_custom_style'];
    } elseif(get_post_meta($id, 'page_page_title_start_custom_style', true) !== '') {
        $page_title_start_custom_style = get_post_meta($id, 'page_page_title_start_custom_style', true);
    }

    if(get_post_meta($id, 'page_page_title_data_end', true) == '' && isset($eltd_options['page_title_data_end']) && $eltd_options['page_title_data_end'] !== '') {
        $page_title_data_end = $eltd_options['page_title_data_end'];
    } elseif(get_post_meta($id, 'page_page_title_data_end', true) !== '') {
        $page_title_data_end = get_post_meta($id, 'page_page_title_data_end', true);
    }

    if(get_post_meta($id, 'page_page_title_end_custom_style', true) == '' && isset($eltd_options['page_title_end_custom_style']) && $eltd_options['page_title_end_custom_style'] !== '') {
        $page_title_end_custom_style = $eltd_options['page_title_end_custom_style'];
    } elseif(get_post_meta($id, 'page_page_title_end_custom_style', true) !== '') {
        $page_title_end_custom_style = get_post_meta($id, 'page_page_title_end_custom_style', true);
    }

    $page_title_animation_data = ' data-'.$page_title_data_start.'="'.$page_title_start_custom_style.'" data-'.$page_title_data_end.'="'.$page_title_end_custom_style.'"';
}





//is header transparent not set on current page?
if(get_post_meta($id, "eltd_header_color_transparency_per_page", true) === "" || eltd_is_default_wp_template()) {
	//take global value set in Elated Options
	$header_transparency = esc_attr($eltd_options['header_background_transparency_initial']);
} else {
	//take value set for current page
	$header_transparency = esc_attr(get_post_meta($id, "eltd_header_color_transparency_per_page", true));
}

//is header completely transparent?
$is_header_transparent 	= in_array($header_transparency, $transparent_values_array);

//is header solid?
$is_header_solid		= in_array($header_transparency, $solid_values_array);

//is header some of sticky types and initially hidden
$is_header_initially_hidden = false;
if(isset($eltd_options['header_bottom_appearance']) && ($eltd_options['header_bottom_appearance'] == "stick" || $eltd_options['header_bottom_appearance'] == "stick menu_bottom" || $eltd_options['header_bottom_appearance'] == "stick_with_left_right_menu")){
	if(get_post_meta($id, "eltd_page_hide_initial_sticky", true) !== "") {
		if(get_post_meta($id, "eltd_page_hide_initial_sticky", true) === "yes") {
				$is_header_initially_hidden = true;
		}
	} else if(isset($eltd_options['hide_initial_sticky']) && $eltd_options['hide_initial_sticky'] == 'yes'){
		$is_header_initially_hidden = true;
	}
}

$title_height = 200; // default title height without header height
if($title_type == "breadcrumbs_title") {
    $title_height = 88;
}

if(get_post_meta($id, "eltd_title-height", true) != ""){
	$title_height = esc_attr(get_post_meta($id, "eltd_title-height", true));
}elseif($eltd_options['title_height'] != ''){
	$title_height = esc_attr($eltd_options['title_height']);
}
//is header solid?
if(!$is_header_solid && !$is_header_initially_hidden && $eltd_options['header_bottom_appearance'] != 'regular' && get_post_meta($id, "eltd_enable_content_top_margin", true) != "yes"){
//	if ((isset($eltd_options['center_logo_image']) && $eltd_options['center_logo_image'] == "yes") || $eltd_options['header_bottom_appearance'] == 'fixed_hiding') {
//		if($eltd_options['header_bottom_appearance'] == 'stick menu_bottom'){
//	        $title_height = $title_height + $header_height + $header_top + $logo_height + 20; // 20 is top margin of centered logo
//	    } elseif($eltd_options['header_bottom_appearance'] == 'fixed_hiding' || $eltd_options['header_bottom_appearance'] == 'fixed'){
//	        if(!empty($eltd_options['header_height']) && $eltd_options['header_bottom_appearance'] == "fixed"){
//	        	$title_height = $title_height + $eltd_options['header_height'] + $header_top + $logo_height + 20;
//	        } else {
//	        	$title_height = $title_height + $header_height + $header_top;
//	        }
//	    } else {
//	        $title_height = $title_height + $header_height + $header_top + $logo_height + 20; // 20 is top margin of centered logo
//	    }
//	} else {
//		$title_height = $title_height + $header_height + $header_top;
//	}
    $title_height = $title_height + $header_height_padding;
	//is header semi-transparent?
	if(!$is_header_transparent) {
		$title_calculated_height = $title_height - $header_height_padding;

		if($title_calculated_height < 0) {
			$title_calculated_height = 0;
		}

		//center title between border and end of title section
		$title_holder_height = 'padding-top:' . $header_height_padding . 'px;height:' . ($title_calculated_height) . 'px;';
		$title_subtitle_padding = 'padding-top:' . esc_attr($header_height_padding) . 'px;';
	} else {
		//header is transparent
		$title_holder_height = 'padding-top:'.$header_height_padding.'px;height:'.($title_height - $header_height_padding).'px;';
		$title_subtitle_padding = 'padding-top:'.esc_attr($header_height_padding).'px;';
	}
} else {
	$title_holder_height = 'height:'.$title_height.'px;';
	$title_subtitle_padding = '';
}

$title_background_color = '';
if(get_post_meta($id, "eltd_page-title-background-color", true) != ""){
    $title_background_color = esc_attr(get_post_meta($id, "eltd_page-title-background-color", true));
}else{
    $title_background_color = esc_attr($eltd_options['title_background_color']);
}

$show_title_image = true;
if(get_post_meta($id, "eltd_show-page-title-image", true) == "yes") {
    $show_title_image = false;
}
$eltd_page_title_style = "standard";
if(get_post_meta($id, "eltd_page_title_style", true) != ""){
    $eltd_page_title_style = get_post_meta($id, "eltd_page_title_style", true);
}else{
    if(isset($eltd_options['title_style'])) {
        $eltd_page_title_style = $eltd_options['title_style'];
    } else {
        $eltd_page_title_style = "standard";
    }
}

$animate_title_area = '';
if(get_post_meta($id, "eltd_animate_page_title", true) != ""){
    $animate_title_area = get_post_meta($id, "eltd_animate_page_title", true);
}else{
    $animate_title_area = $eltd_options['animate_title_area'];
}

if($animate_title_area == "text_right_left") {
    $animate_title_class = "animate_title_text";
} elseif($animate_title_area == "area_top_bottom"){
    $animate_title_class = "animate_title_area";
} else {
    $animate_title_class = "title_without_animation";
}

//is vertical menu activated in Elated Options?
if(isset($eltd_options['vertical_area']) && $eltd_options['vertical_area'] =='yes' && isset($eltd_options['paspartu']) && $eltd_options['paspartu'] == 'no'){
    $title_subtitle_padding = 0;
    $title_holder_height = 200;
    if($title_type == "breadcrumbs_title") {
        $title_holder_height = 100;
    }
    $title_height = 200; // default title height without header height
    if($title_type == "breadcrumbs_title") {
        $title_height = 100;
    }
    if(get_post_meta($id, "eltd_title-height", true) != ""){
        $title_holder_height = get_post_meta($id, "eltd_title-height", true);
        $title_height = esc_attr(get_post_meta($id, "eltd_title-height", true));
    }else if($eltd_options['title_height'] != ''){
        $title_holder_height = $eltd_options['title_height'];
        $title_height = esc_attr($eltd_options['title_height']);
    }
    $title_holder_height = 'height:' . esc_attr($title_holder_height). 'px;';
}

$enable_breadcrumbs = 'no';
if(get_post_meta($id, "eltd_enable_breadcrumbs", true) != ""){
	$enable_breadcrumbs = get_post_meta($id, "eltd_enable_breadcrumbs", true);
}elseif(isset($eltd_options['enable_breadcrumbs'])){
	$enable_breadcrumbs = $eltd_options['enable_breadcrumbs'];
}

//Breadcrumbs Animation
$breadcrumbs_animation = 'no';
if (get_post_meta($id, 'page_page_title_breadcrumbs_animations', true) !== '') {
    $breadcrumbs_animation = get_post_meta($id, 'page_page_title_breadcrumbs_animations', true);
}
elseif (get_post_meta($id, 'page_page_title_breadcrumbs_animations', true) == '' && isset($eltd_options['page_title_breadcrumbs_animations']) && $eltd_options['page_title_breadcrumbs_animations'] !== '') {
    $breadcrumbs_animation = $eltd_options['page_title_breadcrumbs_animations'];
}

$page_title_breadcrumbs_animation_data = '';
if($enable_breadcrumbs == 'yes' && $breadcrumbs_animation == 'yes') {

    $page_title_breadcrumbs_data_start = '0';
    $page_title_breadcrumbs_start_custom_style = 'opacity:1';
    $page_title_breadcrumbs_data_end = '300';
    $page_title_breadcrumbs_end_custom_style = 'opacity:0';

    if(get_post_meta($id, 'page_page_title_breadcrumbs_data_start', true) == '' && isset($eltd_options['page_title_breadcrumbs_data_start']) && ($eltd_options['page_title_breadcrumbs_data_start'] !== '')) {
        $page_title_breadcrumbs_data_start = $eltd_options['page_title_breadcrumbs_data_start'];
    } elseif (get_post_meta($id, 'page_page_title_breadcrumbs_data_start', true) !== '') {
        $page_title_breadcrumbs_data_start = get_post_meta($id, 'page_page_title_breadcrumbs_data_start', true);
    }

    if(get_post_meta($id, 'page_page_title_breadcrumbs_start_custom_style', true) == '' && isset($eltd_options['page_title_breadcrumbs_start_custom_style']) && ($eltd_options['page_title_breadcrumbs_start_custom_style'] !== '')) {
        $page_title_breadcrumbs_start_custom_style = $eltd_options['page_title_breadcrumbs_start_custom_style'];
    } elseif (get_post_meta($id, 'page_page_title_breadcrumbs_start_custom_style', true) !== '') {
        $page_title_breadcrumbs_start_custom_style = get_post_meta($id, 'page_page_title_breadcrumbs_start_custom_style', true);
    }

    if(get_post_meta($id, 'page_page_title_breadcrumbs_data_end', true) == '' && isset($eltd_options['page_title_breadcrumbs_data_end']) && ($eltd_options['page_title_breadcrumbs_data_end'] !== '')) {
        $page_title_breadcrumbs_data_end = $eltd_options['page_title_breadcrumbs_data_end'];
    } elseif (get_post_meta($id, 'page_page_title_breadcrumbs_data_end', true) !== '') {
        $page_title_breadcrumbs_data_end = get_post_meta($id, 'page_page_title_breadcrumbs_data_end', true);
    }

    if(get_post_meta($id, 'page_page_title_breadcrumbs_end_custom_style', true) == '' && isset($eltd_options['page_title_breadcrumbs_end_custom_style']) && ($eltd_options['page_title_breadcrumbs_end_custom_style'] !== '')) {
        $page_title_breadcrumbs_end_custom_style = $eltd_options['page_title_breadcrumbs_end_custom_style'];
    } elseif (get_post_meta($id, 'page_page_title_breadcrumbs_end_custom_style', true) !== '') {
        $page_title_breadcrumbs_end_custom_style = get_post_meta($id, 'page_page_title_breadcrumbs_end_custom_style', true);
    }

    $page_title_breadcrumbs_animation_data = ' data-'.$page_title_breadcrumbs_data_start.'="'.$page_title_breadcrumbs_start_custom_style.'" data-'.$page_title_breadcrumbs_data_end.'="'.$page_title_breadcrumbs_end_custom_style.'"';
}

$title_text_shadow = '';
if(get_post_meta($id, "eltd_title_text_shadow", true) != ""){
	if(get_post_meta($id, "eltd_title_text_shadow", true) == "yes"){
		$title_text_shadow = ' title_text_shadow';
	}
}else{
	if($eltd_options['title_text_shadow'] == "yes"){
		$title_text_shadow = ' title_text_shadow';
	}
}
$subtitle_color ="";
if(get_post_meta($id, "eltd_page_subtitle_color", true) != ""){
	$subtitle_color = "color:" . esc_attr(get_post_meta($id, "eltd_page_subtitle_color", true)) . ";";
}

if (is_tag()) {
	$title = single_term_title("", false)." Tag";
}elseif (is_date()) {
	$title = get_the_time('F Y');
}elseif (is_author()){
	$title = __('Author:', 'eltd').get_the_author();
}elseif (is_category()){
	$title = single_cat_title('', false);
}elseif (is_home()){
	$title = get_option('blogname');
}elseif (is_search()){
    $title = __('Results for', 'eltd').': '.get_search_query();
}elseif (is_404()){
	if($eltd_options['404_title'] != "") {
		$title = $eltd_options['404_title'];
	} else {
		$title = __('404 - Page not found', 'eltd');
	}
}elseif(function_exists("is_woocommerce") && (is_shop() || is_singular('product'))){
	global $woocommerce;
	$shop_id = get_option('woocommerce_shop_page_id');
	$shop= get_page($shop_id);
	$title = $shop->post_title;
}elseif(function_exists("is_woocommerce") && (is_product_category() || is_product_tag())){
	global $wp_query;
	$tax = $wp_query->get_queried_object();
	$category_title = $tax->name;
	$title = $category_title;
}elseif (is_archive()){
	$title = __('Archive','eltd');
}else {
	$title = get_the_title($id);
}

$title_shortcode = "";

$title_like_separator = false;

if (get_post_meta($id,"eltd_title_like_separator",true) == "yes" || (isset($eltd_options['title_like_separator']) && $eltd_options['title_like_separator']=='yes' && get_post_meta($id,"eltd_title_like_separator",true) != "no")) {
    $title_like_separator = true;
}

if ($title_like_separator) {

    $title_shortcode .= 'title = "' . $title . '"';

    if (get_post_meta($id,'eltd_page_title_position',true) != "") {
        $title_shortcode .= ' text_position = "' . get_post_meta($id,'eltd_page_title_position',true) . '"';
    }
    elseif (isset($eltd_options['page_title_position']) && !empty($eltd_options['page_title_position'])) {
        $title_shortcode .= ' text_position = "' . $eltd_options['page_title_position'] . '"';
    }

    if (get_post_meta($id, "eltd_page-title-color", true) != "") {
        $title_shortcode .= ' title_color = "' . get_post_meta($id, "eltd_page-title-color", true) . '"';
    }

    if (get_post_meta($id, "eltd_title_like_separator_line_color", true) != "") {
        $title_shortcode .= ' line_color = "' . get_post_meta($id, "eltd_title_like_separator_line_color", true) . '"';
    }
    elseif (isset($eltd_options['title_like_separator_line_color']) && !empty($eltd_options['title_like_separator_line_color'])) {
        $title_shortcode .= ' line_color = "' . $eltd_options['title_like_separator_line_color'] . '"';
    }

    if (get_post_meta($id, "eltd_title_like_separator_line_width", true) != "") {
        $title_shortcode .= ' line_width = "' .   get_post_meta($id, "eltd_title_like_separator_line_width", true) . '"';
    }
    elseif (isset($eltd_options['title_like_separator_line_width']) && !empty($eltd_options['title_like_separator_line_width'])) {
        $title_shortcode .= ' line_width = "' . $eltd_options['title_like_separator_line_width'] . '"';
    }

    if (get_post_meta($id, "eltd_title_like_separator_line_thickness", true) != "") {
        $title_shortcode .= ' line_thickness = "' .   get_post_meta($id, "eltd_title_like_separator_line_thickness", true) . '"';
    }
    elseif (isset($eltd_options['title_like_separator_line_thickness']) && !empty($eltd_options['title_like_separator_line_thickness'])) {
        $title_shortcode .= ' line_thickness = "' . $eltd_options['title_like_separator_line_thickness'] . '"';
    }

    if (get_post_meta($id, "eltd_title_like_separator_line_style", true) != "") {
        $title_shortcode .= ' line_border_style = "' .   get_post_meta($id, "eltd_title_like_separator_line_style", true) . '"';
    }
    elseif (isset($eltd_options['title_like_separator_line_style']) && !empty($eltd_options['title_like_separator_line_style'])) {
        $title_shortcode .= ' line_border_style = "' . $eltd_options['title_like_separator_line_style'] . '"';
    }

    if (get_post_meta($id, "eltd_title_like_separator_margins", true) != "") {
        $title_shortcode .= ' box_margin = "' .   get_post_meta($id, "eltd_title_like_separator_margins", true) . '"';
    }
    elseif (isset($eltd_options['title_like_separator_margins']) && !empty($eltd_options['title_like_separator_margins'])) {
        $title_shortcode .= ' box_margin = "' . $eltd_options['title_like_separator_margins'] . '"';
    }

    $title_like_separator_dots = false;

    if (get_post_meta($id, "eltd_title_like_separator_line_dots", true) != "") {
        $title_shortcode .= ' line_dots = "' .   get_post_meta($id, "eltd_title_like_separator_line_dots", true) . '"';
        if (get_post_meta($id, "eltd_title_like_separator_line_dots", true) == "yes") {
            $title_like_separator_dots = true;
        }
    }
    elseif (isset($eltd_options['title_like_separator_line_dots']) && !empty($eltd_options['title_like_separator_line_dots'])) {
        $title_shortcode .= ' line_dots = "' . $eltd_options['title_like_separator_line_dots'] . '"';
        if ($eltd_options['title_like_separator_line_dots'] == "yes") {
            $title_like_separator_dots = true;
        }
    }


    if ($title_like_separator_dots) {
        if (get_post_meta($id,"eltd_title_like_separator_dots_size", true) != "") {
            $title_shortcode .= ' line_dots_size = "' .   get_post_meta($id,"eltd_title_like_separator_dots_size", true) . '"';
        }
        elseif (isset($eltd_options['title_like_separator_dots_size']) && !empty($eltd_options['title_like_separator_dots_size'])) {
            $title_shortcode .= ' line_dots_size = "' . $eltd_options['title_like_separator_dots_size'] . '"';
        }

        if (get_post_meta($id, "eltd_title_like_separator_dots_color", true) != "") {
            $title_shortcode .= ' line_dots_color = "' .   get_post_meta($id, "eltd_title_like_separator_dots_color", true) . '"';
        }
        elseif (isset($eltd_options['title_like_separator_dots_color']) && !empty($eltd_options['title_like_separator_dots_color'])) {
            $title_shortcode .= ' line_dots_color = "' . $eltd_options['title_like_separator_dots_color'] . '"';
        }
    }

    $title_span_background_color = "";
    $title_span_background_color_rgb = "";
    $title_span_background_color = "";
    $title_in_box = "";

    if (get_post_meta($id,'eltd_title_background_color',true) != "") {
        $title_span_background_color_rgb = eltd_hex2rgb(esc_attr(get_post_meta($id, 'eltd_title_background_color', true)));
    }

    if (get_post_meta($id,'eltd_title_opacity',true) != "") {
        $title_span_opacity = esc_attr(get_post_meta($id,'eltd_title_opacity',true));
    }
    else {
        $title_span_opacity = 1;
    }

    if (is_array($title_span_background_color_rgb) && count($title_span_background_color_rgb)) {
        $title_span_background_color = ' box_background_color = "rgba(' . $title_span_background_color_rgb[0] .','. $title_span_background_color_rgb[1] .','. $title_span_background_color_rgb[2] .','. $title_span_opacity . ')" ';
    }


    if (get_post_meta($id,'eltd_title_right_padding',true) != "") {
        $title_shortcode .= ' box_padding = "' . esc_attr(get_post_meta($id,'eltd_title_right_padding',true)). 'px" ';
    }

    if ($title_span_background_color !== "") {
        $title_shortcode = '[vc_text_separator text_in_box="yes" box_border_color="transparent" ' . $title_shortcode . $title_span_background_color . ']';
        $title_in_box = "title_in_box";
    }
    else {
        $title_shortcode = '[vc_text_separator text_in_box="no" ' . $title_shortcode . ']';
    }


}

$subtitle_shortcode = "";

$subtitle_like_separator = false;

if (get_post_meta($id,"eltd_subtitle_like_separator",true) == "yes" || (isset($eltd_options['subtitle_like_separator']) && $eltd_options['subtitle_like_separator']=='yes' && get_post_meta($id,"eltd_subtitle_like_separator",true) != "no")) {
    $subtitle_like_separator = true;
}

if ($subtitle_like_separator) {

    $subtitle_shortcode .= 'title = "' . get_post_meta($id, "eltd_page_subtitle", true) . '"';

    if (get_post_meta($id,'eltd_page_title_position',true) != "") {
        $subtitle_shortcode .= ' text_position = "' . get_post_meta($id,'eltd_page_title_position',true) . '"';
    }
    elseif (isset($eltd_options['page_title_position']) && !empty($eltd_options['page_title_position'])) {
        $subtitle_shortcode .= ' text_position = "' . $eltd_options['page_title_position'] . '"';
    }

    if (get_post_meta($id, "eltd_page_subtitle_color", true) != "") {
        $subtitle_shortcode .= ' title_color = "' . get_post_meta($id, "eltd_page_subtitle_color", true) . '"';
    }

    if (get_post_meta($id, "eltd_subtitle_like_separator_line_color", true) != "") {
        $subtitle_shortcode .= ' line_color = "' . get_post_meta($id, "eltd_subtitle_like_separator_line_color", true) . '"';
    }
    elseif (isset($eltd_options['subtitle_like_separator_line_color']) && !empty($eltd_options['subtitle_like_separator_line_color'])) {
        $subtitle_shortcode .= ' line_color = "' . $eltd_options['subtitle_like_separator_line_color'] . '"';
    }

    if (get_post_meta($id, "eltd_subtitle_like_separator_line_width", true) != "") {
        $subtitle_shortcode .= ' line_width = "' .   get_post_meta($id, "eltd_subtitle_like_separator_line_width", true) . '"';
    }
    elseif (isset($eltd_options['subtitle_like_separator_line_width']) && !empty($eltd_options['subtitle_like_separator_line_width'])) {
        $subtitle_shortcode .= ' line_width = "' . $eltd_options['subtitle_like_separator_line_width'] . '"';
    }

    if (get_post_meta($id, "eltd_subtitle_like_separator_line_thickness", true) != "") {
        $subtitle_shortcode .= ' line_thickness = "' .   get_post_meta($id, "eltd_subtitle_like_separator_line_thickness", true) . '"';
    }
    elseif (isset($eltd_options['subtitle_like_separator_line_thickness']) && !empty($eltd_options['subtitle_like_separator_line_thickness'])) {
        $subtitle_shortcode .= ' line_thickness = "' . $eltd_options['subtitle_like_separator_line_thickness'] . '"';
    }

    if (get_post_meta($id, "eltd_subtitle_like_separator_line_style", true) != "") {
        $subtitle_shortcode .= ' line_border_style = "' .   get_post_meta($id, "eltd_subtitle_like_separator_line_style", true) . '"';
    }
    elseif (isset($eltd_options['subtitle_like_separator_line_style']) && !empty($eltd_options['subtitle_like_separator_line_style'])) {
        $subtitle_shortcode .= ' line_border_style = "' . $eltd_options['subtitle_like_separator_line_style'] . '"';
    }

    if (get_post_meta($id, "eltd_subtitle_like_separator_margins", true) != "") {
        $subtitle_shortcode .= ' box_margin = "' .   get_post_meta($id, "eltd_subtitle_like_separator_margins", true) . '"';
    }
    elseif (isset($eltd_options['subtitle_like_separator_margins']) && !empty($eltd_options['subtitle_like_separator_margins'])) {
        $subtitle_shortcode .= ' box_margin = "' . $eltd_options['subtitle_like_separator_margins'] . '" ';
    }


    $subtitle_span_background_color = "";
    $subtitle_span_background_color_rgb = "";
    $subtitle_span_background_color = "";
    $subtitle_in_box = "";

    if (get_post_meta($id,'eltd_subtitle_background_color',true) != "") {
        $subtitle_span_background_color_rgb = eltd_hex2rgb(esc_attr(get_post_meta($id, 'eltd_subtitle_background_color', true)));
    }

    if (get_post_meta($id,'eltd_subtitle_opacity',true) != "") {
        $subtitle_span_opacity = esc_attr(get_post_meta($id,'eltd_subtitle_opacity',true));
    }
    else {
        $subtitle_span_opacity = 1;
    }

    if (is_array($subtitle_span_background_color_rgb) && count($subtitle_span_background_color_rgb)) {
        $subtitle_span_background_color = ' box_background_color = "rgba(' . $subtitle_span_background_color_rgb[0] .','. $subtitle_span_background_color_rgb[1] .','. $subtitle_span_background_color_rgb[2] .','. $subtitle_span_opacity . ')" ';
    }

    if (get_post_meta($id,'eltd_subtitle_right_padding',true) != "") {
        $subtitle_shortcode .= ' box_padding = "' . esc_attr(get_post_meta($id,'eltd_subtitle_right_padding',true)). 'px" ';
    }

    if ($subtitle_span_background_color !== "") {
        $subtitle_shortcode = '[vc_text_separator text_in_box="yes" box_border_color="transparent" ' . $subtitle_shortcode . $subtitle_span_background_color . ']';
        $subtitle_in_box = "subtitle_in_box";
    }
    else {
        $subtitle_shortcode = '[vc_text_separator text_in_box="no" ' . $subtitle_shortcode . ']';
    }

}


//Title Separator
$title_separator = "";
$title_separator_style = "";
$separator_title_position = "";
$separator_animation = '';
if (get_post_meta($id,'eltd_title_separator',true) == "yes"  || (!empty($eltd_options['title_separator']) && $eltd_options['title_separator']=='yes' && get_post_meta($id,'eltd_title_separator',true) != "no")) {
    $title_separator = "yes";

    $title_separator_position_vertical = "below";
    if (get_post_meta($id,'eltd_title_separator_position',true) == "above") {
        $title_separator_position_vertical = "above";
    }
    elseif (get_post_meta($id,'eltd_title_separator_position',true) == "below") {
        $title_separator_position_vertical = "below";
    }
    elseif (isset($eltd_options['title_separator_position']) && $eltd_options['title_separator_position'] == "above") {
        $title_separator_position_vertical = "above";
    }

    $with_icon = false;

    if (get_post_meta($id,'eltd_title_separator_format',true) == "with_icon" || get_post_meta($id,'eltd_title_separator_format',true) == "with_custom_icon") {
        $with_icon = true;
    }
    elseif (get_post_meta($id,'eltd_title_separator_format',true) == "normal") {
        $with_icon = false;
    }
    elseif ($eltd_options['title_separator_format'] == "with_icon" || $eltd_options['title_separator_format'] == "with_custom_icon") {
        $with_icon = true;
    }


    if ($with_icon) {

        $separator_shortcode_params = '';

        if (get_post_meta($id,'eltd_title_separator_format',true) != "") {
            $separator_shortcode_params .= ' type = "' . get_post_meta($id,'eltd_title_separator_format',true) . '" ';
        }
        else {
            $separator_shortcode_params .= ' type = "' . $eltd_options['title_separator_format'] . '" ';
        }

        if (get_post_meta($id,'eltd_title_separator_format',true) == "with_custom_icon" || (get_post_meta($id,'eltd_title_separator_format',true) == "" && $eltd_options['title_separator_format'] == "with_custom_icon")) {
            if (get_post_meta($id,'eltd_separator_custom_icon',true) != "") {
                $separator_shortcode_params .= ' custom_icon ="' . get_post_meta($id,'eltd_separator_custom_icon',true) . '" ';
            }
            elseif (isset($eltd_options['separator_custom_icon']) && !empty($eltd_options['separator_custom_icon'])) {
                $separator_shortcode_params .= ' custom_icon ="' . $eltd_options['separator_custom_icon'] . '" ';
            }
        }
        else {
            if (get_post_meta($id,'eltd_separator_icon_pack',true) != "") {
                $separator_shortcode_params .= $eltdIconCollections->iconPackParamName.'="'.get_post_meta($id,'eltd_separator_icon_pack',true).'"';

                $icon_collection = $eltdIconCollections->getIconCollection(get_post_meta($id,'eltd_separator_icon_pack',true));

                if(is_object($icon_collection) && get_post_meta($id,'eltd_separator_icon_'.$icon_collection->param,true) !== "") {
                    $separator_shortcode_params .= ' '.$icon_collection->param.'="'.get_post_meta($id,'eltd_separator_icon_'.$icon_collection->param,true).'"';
                }
            }
            elseif (isset($eltd_options['separator_icon_pack']) && !empty($eltd_options['separator_icon_pack'])) {

                $separator_shortcode_params .= $eltdIconCollections->iconPackParamName.'="'.$eltd_options['separator_icon_pack'].'"';

                $icon_collection = $eltdIconCollections->getIconCollection($eltd_options['separator_icon_pack']);

                if(is_object($icon_collection) && isset($eltd_options['separator_icon_'.$icon_collection->param]) && !empty($eltd_options['separator_icon_'.$icon_collection->param])) {
                    $separator_shortcode_params .= ' '.$icon_collection->param.'="'.$eltd_options['separator_icon_'.$icon_collection->param].'"';
                }
            }

            if (get_post_meta($id,'eltd_title_separator_icon_hover_color',true) != "") {
                $separator_shortcode_params .= ' hover_icon_color = "'.get_post_meta($id,'eltd_title_separator_icon_hover_color',true).'"';
            }
            elseif (isset($eltd_options['title_separator_icon_hover_color']) && !empty($eltd_options['title_separator_icon_hover_color'])) {
                $separator_shortcode_params .= ' hover_icon_color = "'.$eltd_options['title_separator_icon_hover_color'].'"';
            }

            if (get_post_meta($id,'eltd_title_separator_icon_color',true) != "") {
                $separator_shortcode_params .= ' icon_color = "'.get_post_meta($id,'eltd_title_separator_icon_color',true).'"';
            }
            elseif (isset($eltd_options['title_separator_icon_color']) && !empty($eltd_options['title_separator_icon_color'])) {
                $separator_shortcode_params .= ' icon_color = "'.$eltd_options['title_separator_icon_color'].'"';
            }

            if (get_post_meta($id,'eltd_title_separator_icon_custom_size',true) != "") {
               $separator_shortcode_params .= ' icon_custom_size = "'.get_post_meta($id,'eltd_title_separator_icon_custom_size',true).'"';
            }
            elseif (isset($eltd_options['title_separator_icon_custom_size']) && !empty($eltd_options['title_separator_icon_custom_size'])) {
                $separator_shortcode_params .= ' icon_custom_size = "'.$eltd_options['title_separator_icon_custom_size'].'"';
            }

            if (get_post_meta($id,'eltd_title_separator_icon_type',true) != "") {
               $separator_shortcode_params .= ' icon_type = "'.get_post_meta($id,'eltd_title_separator_icon_type',true).'"';
               $separator_type = get_post_meta($id,'eltd_title_separator_icon_type',true);
            }
            elseif (isset($eltd_options['title_separator_icon_type']) && !empty($eltd_options['title_separator_icon_type'])) {
                $separator_shortcode_params .= ' icon_type = "'.$eltd_options['title_separator_icon_type'].'"';
                $separator_type = $eltd_options['title_separator_icon_type'];
            }

            if ($separator_type != "normal"){
                if (get_post_meta($id,'eltd_title_separator_icon_border_radius',true) != "") {
                   $separator_shortcode_params .= ' icon_border_radius = "'.get_post_meta($id,'eltd_title_separator_icon_border_radius',true).'"';
                }
                elseif (isset($eltd_options['title_separator_icon_border_radius']) && !empty($eltd_options['title_separator_icon_border_radius'])) {
                    $separator_shortcode_params .= ' icon_border_radius = "'.$eltd_options['title_separator_icon_border_radius'].'"';
                }

                if (get_post_meta($id,'eltd_title_separator_icon_border_width',true) != "") {
                   $separator_shortcode_params .= ' icon_border_width = "'.get_post_meta($id,'eltd_title_separator_icon_border_width',true).'"';
                }
                elseif (isset($eltd_options['title_separator_icon_border_width']) && !empty($eltd_options['title_separator_icon_border_width'])) {
                    $separator_shortcode_params .= ' icon_border_width = "'.$eltd_options['title_separator_icon_border_width'].'"';
                }

                if (get_post_meta($id,'eltd_title_separator_icon_border_color',true) != "") {
                   $separator_shortcode_params .= ' icon_border_color = "'.get_post_meta($id,'eltd_title_separator_icon_border_color',true).'"';
                }
                elseif (isset($eltd_options['title_separator_icon_border_color']) && !empty($eltd_options['title_separator_icon_border_color'])) {
                    $separator_shortcode_params .= ' icon_border_color = "'.$eltd_options['title_separator_icon_border_color'].'"';
                }

                if (get_post_meta($id,'eltd_title_separator_icon_border_hover_color',true) != "") {
                   $separator_shortcode_params .= ' hover_icon_border_color = "'.get_post_meta($id,'eltd_title_separator_icon_border_hover_color',true).'"';
                }
                elseif (isset($eltd_options['title_separator_icon_border_hover_color']) && !empty($eltd_options['title_separator_icon_border_hover_color'])) {
                    $separator_shortcode_params .= ' hover_icon_border_color = "'.$eltd_options['title_separator_icon_border_hover_color'].'"';
                }

                if (get_post_meta($id,'eltd_title_separator_icon_shape_size',true) != "") {
                   $separator_shortcode_params .= ' icon_shape_size = "'.get_post_meta($id,'eltd_title_separator_icon_shape_size',true).'"';
                }
                elseif (isset($eltd_options['title_separator_icon_shape_size']) && !empty($eltd_options['title_separator_icon_shape_size'])) {
                    $separator_shortcode_params .= ' icon_shape_size = "'.$eltd_options['title_separator_icon_shape_size'].'"';
                }

                if (get_post_meta($id,'eltd_title_separator_icon_background_color',true) != "") {
                   $separator_shortcode_params .= ' icon_background_color = "'.get_post_meta($id,'eltd_title_separator_icon_background_color',true).'"';
                }
                elseif (isset($eltd_options['title_separator_icon_background_color']) && !empty($eltd_options['title_separator_icon_background_color'])) {
                    $separator_shortcode_params .= ' icon_background_color = "'.$eltd_options['title_separator_icon_background_color'].'"';
                }

                if (get_post_meta($id,'eltd_title_separator_icon_background_hover_color',true) != "") {
                   $separator_shortcode_params .= ' hover_icon_background_color = "'.get_post_meta($id,'eltd_title_separator_icon_background_hover_color',true).'"';
                }
                elseif (isset($eltd_options['title_separator_icon_background_hover_color']) && !empty($eltd_options['title_separator_icon_background_hover_color'])) {
                    $separator_shortcode_params .= ' hover_icon_background_color = "'.$eltd_options['title_separator_icon_background_hover_color'].'"';
                }
            }
        }

        if (get_post_meta($id,'eltd_title_separator_color',true) != "") {
           $separator_shortcode_params .= ' color = "'.get_post_meta($id,'eltd_title_separator_color',true).'"';
        }
        elseif (isset($eltd_options['title_separator_color']) && !empty($eltd_options['title_separator_color'])) {
            $separator_shortcode_params .= ' color = "'.$eltd_options['title_separator_color'].'"';
        }

        if (get_post_meta($id,'eltd_title_separator_type',true) != "") {
           $separator_shortcode_params .= ' border_style = "'.get_post_meta($id,'eltd_title_separator_type',true).'"';
        }
        elseif (isset($eltd_options['title_separator_type']) && !empty($eltd_options['title_separator_type'])) {
            $separator_shortcode_params .= ' border_style = "'.$eltd_options['title_separator_type'].'"';
        }

        if (get_post_meta($id,'eltd_title_separator_thickness',true) != "") {
           $separator_shortcode_params .= ' thickness = "'.get_post_meta($id,'eltd_title_separator_thickness',true).'"';
        }
        elseif (isset($eltd_options['title_separator_thickness']) && !empty($eltd_options['title_separator_thickness'])) {
            $separator_shortcode_params .= ' thickness = "'.$eltd_options['title_separator_thickness'].'"';
        }

        if (get_post_meta($id,'eltd_title_separator_width',true) != "") {
           $separator_shortcode_params .= ' width = "'.get_post_meta($id,'eltd_title_separator_width',true).'"';
        }
        elseif (isset($eltd_options['title_separator_width']) && !empty($eltd_options['title_separator_width'])) {
            $separator_shortcode_params .= ' width = "'.$eltd_options['title_separator_width'].'"';
        }

        if (get_post_meta($id,'eltd_title_separator_topmargin',true) != "") {
           $separator_shortcode_params .= ' up = "'.get_post_meta($id,'eltd_title_separator_topmargin',true).'"';
        }
        elseif (isset($eltd_options['title_separator_topmargin']) && !empty($eltd_options['title_separator_topmargin'])) {
            $separator_shortcode_params .= ' up = "'.$eltd_options['title_separator_topmargin'].'"';
        }

        if (get_post_meta($id,'eltd_title_separator_bottommargin',true) != "") {
           $separator_shortcode_params .= ' down = "'.get_post_meta($id,'eltd_title_separator_bottommargin',true).'"';
        }
        elseif (isset($eltd_options['title_separator_bottommargin']) && !empty($eltd_options['title_separator_bottommargin'])) {
            $separator_shortcode_params .= ' down = "'.$eltd_options['title_separator_bottommargin'].'"';
        }

        if (get_post_meta($id,'eltd_title_separator_icon_position',true) != "") {
           $separator_shortcode_params .= ' separator_icon_position = "'.get_post_meta($id,'eltd_title_separator_icon_position',true).'"';
        }
        elseif (isset($eltd_options['title_separator_icon_position']) && !empty($eltd_options['title_separator_icon_position'])) {
            $separator_shortcode_params .= ' separator_icon_position = "'.$eltd_options['title_separator_icon_position'].'"';
        }

        if (get_post_meta($id,'eltd_title_separator_icon_margins',true) != "") {
           $separator_shortcode_params .= ' icon_margin = "'.get_post_meta($id,'eltd_title_separator_icon_margins',true).'"';
        }
        elseif (isset($eltd_options['title_separator_icon_margins']) && !empty($eltd_options['title_separator_icon_margins'])) {
            $separator_shortcode_params .= ' icon_margin = "'.$eltd_options['title_separator_icon_margins'].'"';
        }
    }

    else {
		if(get_post_meta($id,'eltd_page_title_position',true) != "") {
			$separator_title_position = get_post_meta($id,'eltd_page_title_position',true);
		}
		elseif(isset($eltd_options['page_title_position']) && !empty($eltd_options['page_title_position'])) {
			$separator_title_position = $eltd_options['page_title_position'];
		}

		$title_separator_style = '';

		if(get_post_meta($id,'eltd_title_separator_color',true) != ""){
			$title_separator_style .= 'border-color:'.esc_attr(get_post_meta($id,'eltd_title_separator_color',true)).';';
		}
		elseif(!empty($eltd_options['title_separator_color'])){
			$title_separator_style .= 'border-color:'.esc_attr($eltd_options['title_separator_color']).';';
		}

		if(get_post_meta($id,'eltd_title_separator_thickness',true) != "") {
			$title_separator_style .= 'border-width: ' . esc_attr(get_post_meta($id,'eltd_title_separator_thickness',true)) . 'px 0 0;';
		}
		elseif(isset($eltd_options['title_separator_thickness']) && !empty($eltd_options['title_separator_thickness'])) {
			$title_separator_style .= 'border-width: ' . esc_attr($eltd_options['title_separator_thickness']) . 'px 0 0;';
		}

		if(get_post_meta($id,'eltd_title_separator_width',true) != "") {
			$title_separator_style .= 'width: ' . esc_attr(get_post_meta($id,'eltd_title_separator_width',true)) . 'px;';
		}
		elseif(isset($eltd_options['title_separator_width']) && !empty($eltd_options['title_separator_width'])) {
			$title_separator_style .= 'width: ' . esc_attr($eltd_options['title_separator_width']) . 'px;';
		}

		if(get_post_meta($id,'eltd_title_separator_topmargin',true) != "") {
			$title_separator_style .= 'margin-top: ' . esc_attr(get_post_meta($id,'eltd_title_separator_topmargin',true)) . 'px;';
		}
		elseif(isset($eltd_options['title_separator_topmargin']) && !empty($eltd_options['title_separator_topmargin'])) {
			$title_separator_style .= 'margin-top: ' . esc_attr($eltd_options['title_separator_topmargin']) . 'px;';
		}

		if(get_post_meta($id,'eltd_title_separator_bottommargin',true) != "") {
			$title_separator_style .= 'margin-bottom: ' . esc_attr(get_post_meta($id,'eltd_title_separator_bottommargin',true)) . 'px;';
		}
		elseif(isset($eltd_options['title_separator_bottommargin']) && !empty($eltd_options['title_separator_bottommargin'])) {
			$title_separator_style .= 'margin-bottom: ' . esc_attr($eltd_options['title_separator_bottommargin']) . 'px;';
		}

		if(get_post_meta($id,'eltd_title_separator_type',true) != "") {
			$title_separator_style .= 'border-style: ' . esc_attr(get_post_meta($id,'eltd_title_separator_type',true)) . ';';
		}
		elseif(isset($eltd_options['title_separator_type']) && !empty($eltd_options['title_separator_type'])) {
			$title_separator_style .= 'border-style: ' . esc_attr($eltd_options['title_separator_type']) . ';';
		}
    }

    //Skrollr animation for separator
    $separator_animation = 'no';
    if (get_post_meta($id, 'page_page_title_separator_animations', true) !== '') {
        $separator_animation = get_post_meta($id, 'page_page_title_separator_animations', true);
    }
    elseif($eltd_options['page_title_separator_animations'] && $eltd_options['page_title_separator_animations'] !== '') {
        $separator_animation = $eltd_options['page_title_separator_animations'];
    }

    $page_title_separator_animation_data = "";
    if($separator_animation == 'yes') {

        $page_title_separator_data_start = '0';
        $page_title_separator_start_custom_style = 'opacity:1';
        $page_title_separator_data_end = '300';
        $page_title_separator_end_custom_style = 'opacity:0';

        if(get_post_meta($id, 'page_page_title_separator_data_start', true) == '' && isset($eltd_options['page_title_separator_data_start']) && $eltd_options['page_title_separator_data_start'] !== '') {
            $page_title_separator_data_start = $eltd_options['page_title_separator_data_start'];
        } elseif (get_post_meta($id, 'page_page_title_separator_data_start', true) !== '') {
            $page_title_separator_data_start = get_post_meta($id, 'page_page_title_separator_data_start', true);
        }

        if(get_post_meta($id, 'page_page_title_separator_start_custom_style', true) == '' && isset($eltd_options['page_title_separator_start_custom_style']) && $eltd_options['page_title_separator_start_custom_style'] !== '') {
            $page_title_separator_start_custom_style = $eltd_options['page_title_separator_start_custom_style'];
        } elseif (get_post_meta($id, 'page_page_title_separator_start_custom_style', true) !== '') {
            $page_title_separator_start_custom_style = get_post_meta($id, 'page_page_title_separator_start_custom_style', true);
        }

        if(get_post_meta($id, 'page_page_title_separator_data_end', true) == '' && isset($eltd_options['page_title_separator_data_end']) && $eltd_options['page_title_separator_data_end'] !== '') {
            $page_title_separator_data_end = $eltd_options['page_title_separator_data_end'];
        } elseif (get_post_meta($id, 'page_page_title_separator_data_end', true) !== '') {
            $page_title_separator_data_end = get_post_meta($id, 'page_page_title_separator_data_end', true);
        }

        if(get_post_meta($id, 'page_page_title_separator_end_custom_style', true) == '' && isset($eltd_options['page_title_separator_end_custom_style']) && $eltd_options['page_title_separator_end_custom_style'] !== '') {
            $page_title_separator_end_custom_style = $eltd_options['page_title_separator_end_custom_style'];
        } elseif (get_post_meta($id, 'page_page_title_separator_end_custom_style', true) !== '') {
            $page_title_separator_end_custom_style = get_post_meta($id, 'page_page_title_separator_end_custom_style', true);
        }
        $page_title_separator_animation_data = ' data-'.$page_title_separator_data_start.'="'.$page_title_separator_start_custom_style.'" data-'.$page_title_separator_data_end.'="'.$page_title_separator_end_custom_style.'"';
    }

}

$title_area_content_background_color = "";
$title_area_content_opacity = "";
$title_area_content_background_color_rgb = array();


if (get_post_meta($id,'eltd_title_area_content_background_color',true) != "") {
    $title_area_content_background_color_rgb = eltd_hex2rgb(esc_attr(get_post_meta($id, 'eltd_title_area_content_background_color', true)));
}

if (get_post_meta($id,'eltd_title_area_content_opacity',true) != "") {
        $title_area_content_opacity = esc_attr(get_post_meta($id,'eltd_title_area_content_opacity',true));
}
else {
    $title_area_content_opacity = 1;
}

if (is_array($title_area_content_background_color_rgb) && count($title_area_content_background_color_rgb)) {
    $title_area_content_background_color = 'background-color: rgba(' . $title_area_content_background_color_rgb[0] .','. $title_area_content_background_color_rgb[1] .','. $title_area_content_background_color_rgb[2] .','. $title_area_content_opacity . ');';
}

$title_span_style = "";
$title_span_background_color = "";
$title_span_opacity = "";
$title_span_padding = "";
$title_span_background_color_rgb = array();


if (get_post_meta($id,'eltd_title_background_color',true) != "") {
    $title_span_background_color_rgb = eltd_hex2rgb(esc_attr(get_post_meta($id, 'eltd_title_background_color', true)));
}

if (get_post_meta($id,'eltd_title_opacity',true) != "") {
    $title_span_opacity = esc_attr(get_post_meta($id,'eltd_title_opacity',true));
}
else {
    $title_span_opacity = 1;
}

if (isset($title_span_background_color_rgb) && count($title_span_background_color_rgb)) {
    $title_span_background_color = 'background-color: rgba(' . $title_span_background_color_rgb[0] .','. $title_span_background_color_rgb[1] .','. $title_span_background_color_rgb[2] .','. $title_span_opacity . ');';
}

if (get_post_meta($id,'eltd_title_top_padding',true) != "") {
    $title_span_padding .= 'padding-top: ' . esc_attr(get_post_meta($id,'eltd_title_top_padding',true)). 'px; ';
}
if (get_post_meta($id,'eltd_title_right_padding',true) != "") {
    $title_span_padding .= 'padding-right: ' . esc_attr(get_post_meta($id,'eltd_title_right_padding',true)). 'px; ';
}
if (get_post_meta($id,'eltd_title_bottom_padding',true) != "") {
    $title_span_padding .= 'padding-bottom: ' . esc_attr(get_post_meta($id,'eltd_title_bottom_padding',true)). 'px; ';
}
if (get_post_meta($id,'eltd_title_left_padding',true) != "") {
    $title_span_padding .= 'padding-left: ' . esc_attr(get_post_meta($id,'eltd_title_left_padding',true)). 'px; ';
}

if ($title_span_background_color != "" || $title_span_padding != "") {
    $title_span_style = $title_span_background_color . $title_span_padding;
}

//Scroll Animation for subtitle, first check if subtitle is enabled
$subtitle_animation = '';
if (get_post_meta($id, 'eltd_page_subtitle', true) !== '') {

    $subtitle_animation = 'no';
    if (get_post_meta($id, 'page_page_subtitle_animations', true) !== '') {
        $subtitle_animation = get_post_meta($id, 'page_page_subtitle_animations', true);
    }
    elseif (isset($eltd_options['page_subtitle_animations']) && $eltd_options['page_subtitle_animations'] !== '') {
        $subtitle_animation = $eltd_options['page_subtitle_animations'];
    }

    $page_subtitle_animation_data = "";
    if ($subtitle_animation == 'yes') {

        $page_subtitle_data_start = '0';
        $page_subtitle_start_custom_style = 'opacity:1';
        $page_subtitle_data_end = '300';
        $page_subtitle_end_custom_style = 'opacity:0';

        if(get_post_meta($id, 'page_page_subtitle_data_start', true) == '' && isset($eltd_options['page_subtitle_data_start']) && ($eltd_options['page_subtitle_data_start'] !== '')) {
            $page_subtitle_data_start = $eltd_options['page_subtitle_data_start'];
        } elseif (get_post_meta($id, 'page_page_subtitle_data_start', true) !== '') {
            $page_subtitle_data_start = get_post_meta($id, 'page_page_subtitle_data_start', true);
        }

        if(get_post_meta($id, 'page_page_subtitle_start_custom_style', true) == '' && isset($eltd_options['page_subtitle_start_custom_style']) && ($eltd_options['page_subtitle_start_custom_style'] !== '')) {
            $page_subtitle_start_custom_style = $eltd_options['page_subtitle_start_custom_style'];
        } elseif (get_post_meta($id, 'page_page_subtitle_start_custom_style', true) !== '') {
            $page_subtitle_start_custom_style = get_post_meta($id, 'page_page_subtitle_start_custom_style', true);
        }

        if(get_post_meta($id, 'page_page_subtitle_data_end', true) == '' && isset($eltd_options['page_subtitle_data_end']) && ($eltd_options['page_subtitle_data_end'] !== '')) {
            $page_subtitle_data_end = $eltd_options['page_subtitle_data_end'];
        } elseif (get_post_meta($id, 'page_page_subtitle_data_end', true) !== '') {
            $page_subtitle_data_end = get_post_meta($id, 'page_page_subtitle_data_end', true);
        }

        if(get_post_meta($id, 'page_page_subtitle_end_custom_style', true) == '' && isset($eltd_options['page_subtitle_end_custom_style']) && ($eltd_options['page_subtitle_end_custom_style'] !== '')) {
            $page_subtitle_end_custom_style = $eltd_options['page_subtitle_end_custom_style'];
        } elseif (get_post_meta($id, 'page_page_subtitle_end_custom_style', true) !== '') {
            $page_subtitle_end_custom_style = get_post_meta($id, 'page_page_subtitle_end_custom_style', true);
        }

        $page_subtitle_animation_data = ' data-'.$page_subtitle_data_start.'="'.$page_subtitle_start_custom_style.'" data-'.$page_subtitle_data_end.'="'.$page_subtitle_end_custom_style.'"';

    }


}

$subtitle_span_style = "";
$subtitle_span_background_color = "";
$subtitle_span_opacity = "";
$subtitle_span_padding = "";
$subtitle_span_background_color_rgb = array();

if (get_post_meta($id,'eltd_subtitle_background_color',true) != "") {
    $subtitle_span_background_color_rgb = eltd_hex2rgb(esc_attr(get_post_meta($id, 'eltd_subtitle_background_color', true)));
}

if (get_post_meta($id,'eltd_subtitle_opacity',true) != "") {
    $subtitle_span_opacity = esc_attr(get_post_meta($id,'eltd_subtitle_opacity',true));
}
else {
    $subtitle_span_opacity = 1;
}

if (is_array($subtitle_span_background_color_rgb) && count($subtitle_span_background_color_rgb)) {
    $subtitle_span_background_color = 'background-color: rgba(' . $subtitle_span_background_color_rgb[0] .','. $subtitle_span_background_color_rgb[1] .','. $subtitle_span_background_color_rgb[2] .','. $subtitle_span_opacity . ');';
}

if (get_post_meta($id,'eltd_subtitle_top_padding',true) != "") {
    $subtitle_span_padding .= 'padding-top: ' . esc_attr(get_post_meta($id,'eltd_subtitle_top_padding',true)). 'px; ';
}
if (get_post_meta($id,'eltd_subtitle_right_padding',true) != "") {
    $subtitle_span_padding .= 'padding-right: ' . esc_attr(get_post_meta($id,'eltd_subtitle_right_padding',true)). 'px; ';
}
if (get_post_meta($id,'eltd_subtitle_bottom_padding',true) != "") {
    $subtitle_span_padding .= 'padding-bottom: ' . esc_attr(get_post_meta($id,'eltd_subtitle_bottom_padding',true)). 'px; ';
}
if (get_post_meta($id,'eltd_subtitle_left_padding',true) != "") {
    $subtitle_span_padding .= 'padding-left: ' . esc_attr(get_post_meta($id,'eltd_subtitle_left_padding',true)). 'px; ';
}

if ($subtitle_span_background_color != "" || $subtitle_span_padding != "") {
    $subtitle_span_style = esc_attr($subtitle_span_background_color . $subtitle_span_padding);
}

//Check if background color should be put on title_subtitle_holder (if there is no title_subtitle_holder_inner)
$title_content_classes = "";
if(!(($responsive_title_image == 'yes' && $show_title_image == true) || ($fixed_title_image == "yes" || $fixed_title_image == "yes_zoom") || ($responsive_title_image == 'no' && $title_image != "" && $fixed_title_image == "no" && $show_title_image == true))){
    $title_content_classes = "title_content_background";
}


$title_classes = '';

if(get_post_meta($id, "eltd_show_page_title_text", true) == 'no') {
	$title_classes = 'without_title_text';
}
$animation = '';
if($title_content_animation == 'yes' || $graphic_animation == 'yes' || $title_animation == 'yes' || $separator_animation == 'yes' || $subtitle_animation == 'yes' || $breadcrumbs_animation == 'yes') {
    $animation = 'data-animation=yes';
}

if($is_title_area_visible) { ?>
	<div class="title_outer <?php echo esc_attr($animate_title_class.$title_text_shadow); if($responsive_title_image == 'yes' && $show_title_image == true && $title_image !== ''){ echo ' with_image'; }?>" <?php echo esc_attr($animation); ?> <?php echo 'data-height="'.esc_attr($title_height).'"'; if($title_height != '' && $animate_title_area == 'area_top_bottom'){ echo ' style="opacity:0; height:0px;"'; } ?>>

    <?php 
        $temp_cats = get_the_terms( $post, 'category' );
        $temp_post_cat = array();
        foreach ($temp_cats as $temp_cat) {
            $temp_post_cat[] = $temp_cat->name;
        }

        if(in_array('provoke weekly', $temp_post_cat)){
    ?>
    		<div class="logo_provoke_weekly">
    			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    				 width="179.9px" height="177.3px" viewBox="0 0 179.9 177.3" enable-background="new 0 0 179.9 177.3" xml:space="preserve">
    			<g>
    				<path fill="#6B6B6B" d="M171.4,103c0.5-2.7,2.6-5.2,4.6-7.5c2-2.3,4-4.7,4-6.9c0-2.2-2-4.6-4-6.9c-2-2.3-4.1-4.8-4.6-7.5
    					c-0.5-2.8,0.6-5.9,1.8-8.8c1-2.8,2.1-5.6,1.4-7.6c-0.7-2-3.4-3.5-6.1-5c-2.7-1.5-5.5-3.1-6.9-5.5c-1.4-2.5-1.4-5.7-1.4-8.8
    					c0-3,0.1-6.1-1.3-7.7c-1.4-1.6-4.4-2.1-7.4-2.6c-3.1-0.5-6.3-1-8.4-2.9c-2.2-1.8-3.2-4.9-4.3-7.8c-1-2.8-2-5.8-3.9-6.8
    					c-0.7-0.4-1.6-0.6-2.8-0.6c-1.5,0-3.2,0.3-5,0.7c-1.9,0.3-3.8,0.7-5.6,0.7c-1.3,0-2.3-0.2-3.3-0.5c-2.6-1-4.7-3.4-6.6-5.8
    					c-1.9-2.3-3.9-4.7-6-5.1c-2.1-0.4-4.7,1.2-7.2,2.7C95.7,4.4,92.9,6,90,6c-2.9,0-5.7-1.7-8.4-3.3c-2.5-1.5-5.1-3.1-7.2-2.7
    					c-2.1,0.4-4.1,2.8-6,5.1c-2,2.4-4,4.9-6.6,5.8c-1,0.4-2,0.5-3.3,0.5c-1.8,0-3.8-0.4-5.6-0.7c-1.8-0.3-3.5-0.7-5-0.7
    					c-1.2,0-2.1,0.2-2.8,0.6c-1.9,1.1-2.9,4-3.9,6.8c-1,2.9-2.1,6-4.3,7.8c-2.2,1.8-5.4,2.3-8.4,2.9c-3,0.5-6,1-7.4,2.6
    					c-1.4,1.6-1.3,4.7-1.3,7.7c0,3.1,0.1,6.4-1.4,8.8c-1.4,2.4-4.2,4-6.9,5.5c-2.6,1.5-5.3,3-6.1,5c-0.7,2,0.4,4.8,1.4,7.6
    					c1.1,2.9,2.2,6,1.8,8.8C8.1,77,6,79.5,4,81.8c-2,2.3-4,4.7-4,6.9c0,2.2,2,4.6,4,6.9c2,2.3,4.1,4.8,4.6,7.5c0.5,2.8-0.6,5.9-1.8,8.8
    					c-1,2.8-2.1,5.7-1.4,7.6c0.7,2,3.4,3.5,6.1,5c2.7,1.5,5.5,3.1,6.9,5.5c1.4,2.5,1.4,5.7,1.4,8.8c0,3-0.1,6.1,1.3,7.7
    					c1.4,1.6,4.4,2.1,7.4,2.6c3.1,0.5,6.3,1,8.4,2.9c2.2,1.8,3.2,4.9,4.3,7.8c1,2.8,2,5.8,3.9,6.8c0.7,0.4,1.6,0.6,2.8,0.6
    					c1.5,0,3.2-0.3,5-0.7c1.9-0.3,3.8-0.7,5.6-0.7c1.3,0,2.3,0.2,3.3,0.5c2.6,1,4.7,3.4,6.6,5.8c1.9,2.3,3.9,4.7,6,5.1
    					c2.1,0.4,4.7-1.2,7.2-2.7c2.7-1.6,5.5-3.3,8.4-3.3c2.9,0,5.7,1.7,8.4,3.3c2.5,1.5,5.1,3.1,7.2,2.7c2.1-0.4,4.1-2.8,6-5.1
    					c2-2.4,4-4.9,6.6-5.8c1-0.4,2-0.5,3.3-0.5c1.8,0,3.8,0.4,5.6,0.7c1.8,0.3,3.5,0.7,5,0.7c1.2,0,2.1-0.2,2.8-0.6
    					c1.9-1.1,2.9-4,3.9-6.8c1-2.9,2.1-6,4.3-7.8c2.2-1.8,5.4-2.3,8.4-2.9c3-0.5,6-1,7.4-2.6c1.4-1.6,1.3-4.7,1.3-7.7
    					c0-3.1-0.1-6.4,1.4-8.8c1.4-2.4,4.2-4,6.9-5.5c2.6-1.5,5.3-3,6.1-5c0.7-2-0.4-4.8-1.4-7.6C172,108.9,170.9,105.9,171.4,103z
    					 M90,167.4c-43.4,0-78.8-35.3-78.8-78.8C11.2,45.2,46.5,9.9,90,9.9c43.4,0,78.8,35.3,78.8,78.8C168.7,132.1,133.4,167.4,90,167.4z"
    					/>
    				<path fill="#6B6B6B" d="M90,13.1c-41.7,0-75.6,33.9-75.6,75.6c0,41.7,33.9,75.6,75.6,75.6c41.7,0,75.6-33.9,75.6-75.6
    					C165.6,47,131.7,13.1,90,13.1z M90,162.6c-40.8,0-73.9-33.2-73.9-73.9S49.2,14.8,90,14.8c40.8,0,73.9,33.2,73.9,73.9
    					S130.7,162.6,90,162.6z"/>
    				<path fill="#6B6B6B" d="M103.6,48.8c-1.6,0.4-3,2.1-3,3.7v19c0,1.3,1,2,2.3,1.7c1.6-0.4,3-2.1,3-3.7v-19c0-1.1-0.6-1.7-1.6-1.7
    					C104.1,48.7,103.9,48.7,103.6,48.8z"/>
    				<path fill="#6B6B6B" d="M134.2,115.7c0,0.9-0.7,1.6-1.6,1.6c-0.9,0-1.6-0.7-1.6-1.6v-37l-12.5,3.1v19.4l5.3-13.1
    					c0.3-0.8,1.3-1.2,2.1-0.9c0.8,0.3,1.2,1.3,0.9,2.1l-5.5,13.6c0.1,0.1,0.1,0.2,0.2,0.3l5.2,13.6c0.3,0.8-0.1,1.7-0.9,2.1
    					c-0.2,0.1-0.4,0.1-0.6,0.1c-0.6,0-1.2-0.4-1.5-1l-4.2-10.8l-1,2.6c0,0,0,0,0,0v9.1c0,0.9-0.7,1.6-1.6,1.6c-0.9,0-1.6-0.7-1.6-1.6
    					V82.5l-45.1,11l-4.6,25.6l6.8,18.6l7.1-38.8c0.2-0.9,1-1.4,1.8-1.3c0.9,0.2,1.4,1,1.3,1.8L74.5,144c-0.1,0.7-0.7,1.2-1.4,1.3
    					c0,0-0.1,0-0.1,0c-0.7,0-1.3-0.4-1.5-1l-7-19l-3.9,21.5c-0.1,0.7-0.7,1.2-1.4,1.3c0,0-0.1,0-0.1,0c-0.7,0-1.3-0.4-1.5-1
    					c-0.4-1.1-10.1-27.9-12.2-34.5c-0.6-1.9-4-11.5-10.3-9.8L21.7,107c8.1,30.1,35.7,52.4,68.3,52.4c39,0,70.7-31.7,70.7-70.7
    					c0-5.8-0.7-11.4-2-16.8l-24.5,6V115.7z M91.1,126.8c-0.5,0.1-1,0.2-1.5,0.2c-1.1,0-2.1-0.3-2.9-1c-1.1-0.9-1.8-2.3-1.8-4v-2.3
    					c0-0.1,0-0.2,0-0.4c0-0.2,0-0.4,0-0.6v-15.8c0-3.1,2.4-6.1,5.4-6.8c1.6-0.4,3.2-0.1,4.4,0.8c1.1,0.9,1.8,2.3,1.8,3.9v5.5
    					c0,3.5-2.8,6.3-5.3,8.7c-1.4,1.4-2.9,2.9-3.2,4v2.9c0,0.7,0.2,1.2,0.6,1.5c0.4,0.3,1,0.4,1.7,0.2c1.9-0.5,3.4-2.4,3.4-4.3
    					c0-0.9,0.7-1.6,1.6-1.6s1.6,0.7,1.6,1.6C97,122.8,94.4,126,91.1,126.8z M105.6,122.9c-0.5,0.1-1,0.2-1.5,0.2c-1.1,0-2.1-0.3-2.9-1
    					c-1.1-0.9-1.8-2.3-1.8-4v-2.3c0-0.1,0-0.2,0-0.4c0-0.2,0-0.4,0-0.6V99.2c0-3.1,2.4-6.1,5.4-6.8c1.6-0.4,3.2-0.1,4.4,0.8
    					c1.1,0.9,1.8,2.3,1.8,3.9v5.5c0,3.5-2.8,6.3-5.3,8.7c-1.4,1.4-2.9,2.9-3.2,4v2.9c0,0.7,0.2,1.2,0.6,1.5c0.4,0.3,1,0.4,1.7,0.2
    					c1.9-0.5,3.4-2.4,3.4-4.3c0-0.9,0.7-1.6,1.6-1.6c0.9,0,1.6,0.7,1.6,1.6C111.5,118.9,108.9,122.1,105.6,122.9z M138.8,84.7
    					c0-0.9,0.7-1.6,1.6-1.6c0.9,0,1.6,0.7,1.6,1.6v14.5c0,1.7,1.5,2.6,3.9,3.8c0.5,0.2,0.9,0.5,1.4,0.7V82.7c0-0.9,0.7-1.6,1.6-1.6
    					c0.9,0,1.6,0.7,1.6,1.6v25.1c0,0.1,0,0.2,0,0.4c0,0.1,0,0.1,0,0.2v8.3c0,3.2-2.3,6-5.4,6.8c-0.5,0.1-1,0.2-1.4,0.2
    					c-1.2,0-2.3-0.4-3.2-1.1c-1.2-1-1.9-2.5-1.9-4.2c0-0.9,0.7-1.6,1.6-1.6s1.6,0.7,1.6,1.6c0,0.7,0.3,1.4,0.7,1.7
    					c0.5,0.4,1.2,0.5,1.9,0.3c2-0.5,3-2.2,3-3.7v-8.7c-0.2-0.7-1.5-1.3-2.8-2c-2.4-1.2-5.7-2.8-5.7-6.7V84.7z"/>
    				<path fill="#6B6B6B" d="M93.4,106.5V101c0-0.6-0.2-1.1-0.6-1.4c-0.3-0.2-0.6-0.3-1-0.3c-0.2,0-0.4,0-0.6,0.1c-1.6,0.4-3,2.1-3,3.7
    					v10.8c0.3-0.3,0.6-0.6,1-1C91.2,110.8,93.4,108.7,93.4,106.5z"/>
    				<path fill="#6B6B6B" d="M135,48.9v-5.5c0-0.6-0.2-1.1-0.6-1.4c-0.3-0.2-0.6-0.3-1-0.3c-0.2,0-0.4,0-0.6,0.1c-1.6,0.4-3,2.1-3,3.7
    					v10.8c0.3-0.3,0.6-0.6,1-1C132.8,53.3,135,51.1,135,48.9z"/>
    				<polygon fill="#6B6B6B" points="66.9,94.3 57.4,96.6 63.5,113.2 	"/>
    				<path fill="#6B6B6B" d="M47.1,69c0-1.3-0.1-7.1-0.1-11.6c0-3-0.1-5.5-0.1-5.8c0-2.6-0.4-4.3-1.2-4.8c-1-0.7-3.5,0.1-6.2,1.3v30.4
    					C43,77.3,47.1,75.5,47.1,69z"/>
    				<path fill="#6B6B6B" d="M107.9,102.6v-5.5c0-0.6-0.2-1.1-0.6-1.4c-0.3-0.2-0.6-0.3-1-0.3c-0.2,0-0.4,0-0.6,0.1
    					c-1.6,0.4-3,2.1-3,3.7V110c0.3-0.3,0.6-0.6,1-1C105.7,106.9,107.9,104.7,107.9,102.6z"/>
    				<path fill="#6B6B6B" d="M73.2,80.4c1.6-0.4,3-2.1,3-3.7v-19c0-1.1-0.6-1.7-1.6-1.7c-0.2,0-0.4,0-0.6,0.1c-1.6,0.4-3,2.1-3,3.7v19
    					C71,80,71.9,80.7,73.2,80.4z"/>
    				<path fill="#6B6B6B" d="M35.1,46.5c5.2-2.4,9.4-4.3,12.2-2.5c1.8,1.2,2.7,3.6,2.7,7.5c0,0.4,0,2.8,0.1,5.8
    					c0.1,4.6,0.1,10.3,0.1,11.6c0,9.4-7,11.7-10.8,12.7v18.2c4.3,1.6,7.3,6.6,9,11.7c1.5,4.7,7.1,20.3,10.1,28.8l3.8-21l-8.4-23
    					c-0.1-0.3-0.1-0.6,0-0.9c0-0.7,0.5-1.4,1.2-1.5l102.7-25.1c-6.3-21.5-22.5-38.7-43.3-46.5c0.3,0.3,0.4,0.6,0.4,1v26.5l7.3-15.6
    					c0.4-0.8,1.3-1.1,2.1-0.8c0.8,0.4,1.1,1.3,0.8,2.1l-6.9,14.7c0,0,0,0,0,0l6.3,19.2c0.3,0.8-0.2,1.7-1,2c-0.2,0.1-0.3,0.1-0.5,0.1
    					c-0.7,0-1.3-0.4-1.5-1.1l-5.3-16.1l-1.4,2.9v15.3c0,0.9-0.7,1.6-1.6,1.6c-0.9,0-1.6-0.7-1.6-1.6V23.4c0-0.8,0.6-1.4,1.4-1.5
    					c-7.3-2.5-15.1-3.9-23.2-3.9c-23.8,0-44.8,11.8-57.6,29.8l0.1,0C33.3,47.3,34.3,46.9,35.1,46.5z M126.5,61.3V45.5
    					c0-3.1,2.4-6.1,5.4-6.8c1.6-0.4,3.2-0.1,4.4,0.8c1.1,0.9,1.8,2.3,1.8,3.9v5.5c0,3.5-2.8,6.3-5.3,8.7c-1.4,1.4-2.9,2.9-3.2,4v2.9
    					c0,0.7,0.2,1.2,0.6,1.5c0.4,0.3,1,0.4,1.7,0.2c1.9-0.5,3.4-2.4,3.4-4.3c0-0.9,0.7-1.6,1.6-1.6c0.9,0,1.6,0.7,1.6,1.6
    					c0,3.3-2.6,6.6-5.8,7.4c-0.5,0.1-1,0.2-1.5,0.2c-1.1,0-2.1-0.3-2.9-1c-1.1-0.9-1.8-2.3-1.8-4v-2.3c0-0.1,0-0.2,0-0.4
    					C126.5,61.7,126.5,61.5,126.5,61.3z M109.1,50.4v19c0,3.1-2.4,6.1-5.4,6.8c-0.5,0.1-0.9,0.2-1.4,0.2c-2.8,0-4.8-2.1-4.8-4.9v-19
    					c0-3.1,2.4-6.1,5.4-6.8c0.5-0.1,0.9-0.2,1.4-0.2C107.1,45.5,109.1,47.6,109.1,50.4z M81.7,50.2c1.2,0,3,0.1,7.1,19.1
    					c1.3-7.6,3.1-17.9,4-21.1c1.6-6,8.8-9.4,11.8-8.8c0.9,0.2,1.4,1,1.2,1.9c-0.2,0.9-1,1.4-1.9,1.2c-1.3-0.2-6.9,2.1-8.1,6.5
    					c-1.3,4.9-5.3,29-5.3,29.2c-0.1,0.8-0.8,1.3-1.5,1.3c0,0,0,0,0,0c-0.8,0-1.4-0.5-1.6-1.3c-2.1-10.9-5.1-23.1-6.5-25
    					c-0.6-0.3-0.9-0.8-0.9-1.5C80.1,50.9,80.9,50.2,81.7,50.2z M67.8,59.7c0-3.1,2.4-6.1,5.4-6.8c0.5-0.1,0.9-0.2,1.4-0.2
    					c2.8,0,4.8,2.1,4.8,4.9v19c0,3.1-2.4,6.1-5.4,6.8c-0.5,0.1-0.9,0.2-1.4,0.2c-2.8,0-4.8-2.1-4.8-4.9V59.7z M53,58.9
    					c1.2-0.3,2.2-0.1,3,0.5c0.2,0.2,0.4,0.4,0.6,0.6c0.4-0.9,0.9-1.5,1.4-2c2.1-1.8,5.2-2.1,6.9-0.8c0.7,0.5,2.1,2,0.6,5.1
    					c-0.4,0.8-1.3,1.1-2.1,0.8c-0.8-0.4-1.1-1.3-0.8-2.1c0.4-0.8,0.3-1.2,0.3-1.2c-0.2-0.2-1.7-0.3-2.9,0.8c-0.9,0.7-2.3,4-2.3,18.4
    					v0.4v6.8c0,0.9-0.7,1.6-1.6,1.6c-0.9,0-1.6-0.7-1.6-1.6v-6.8l0-0.4c0,0,0-0.1,0-0.1V65.7c0-2.8-0.4-3.6-0.5-3.8c0,0-0.1,0-0.3,0.1
    					c-0.9,0.2-1.7-0.3-1.9-1.2C51.6,59.9,52.2,59.1,53,58.9z"/>
    				<path fill="#6B6B6B" d="M36.3,99.4v-50c-0.8,0.4-1.7,0.8-2.6,1.2l-4.3,1.8c0,0-0.1,0-0.1,0c-6.4,10.6-10,23-10,36.2
    					c0,5.2,0.6,10.4,1.7,15.3l13.3-4.2C35,99.5,35.6,99.4,36.3,99.4z"/>
    			</g>
    			</svg>
    		</div>
    <?php 
        }
    ?>

		<div class="title <?php eltd_title_classes(); ?>" style="<?php if($responsive_title_image == 'no' && $title_image != "" && $show_title_image == true){ if($title_image_width != ''){ echo 'background-size:'.esc_attr($title_image_width).'px auto;'; } echo 'background-image:url('.esc_url($title_image).');';  } if($title_height != ''){ echo 'height:'.esc_attr($title_height).'px;'; } if($title_background_color != ''){ echo 'background-color:'.esc_attr($title_background_color).';'; } ?>">
			<div class="image <?php if($responsive_title_image == 'yes' && $title_image != "" && $show_title_image == true){ echo "responsive"; }else{ echo "not_responsive"; } ?>"><?php if($title_image != ""){ ?><img src="<?php echo esc_url($title_image); ?>" alt="&nbsp;" /> <?php } ?></div>
			<?php if($title_overlay_image != ""){ ?>
				<div class="title_overlay" style="background-image:url('<?php echo esc_url($title_overlay_image); ?>');"></div>
			<?php } ?>

    			<div class="title_holder" <?php print $page_title_content_animation_data; ?> <?php if($responsive_title_image != 'yes'){ eltd_inline_style($title_holder_height); } ?>>
    				<div class="container">


    					<div class="container_inner clearfix">
    						<div class="title_subtitle_holder <?php echo esc_attr($title_content_classes); ?>" <?php if($title_image !== '' && $responsive_title_image == 'yes' && $show_title_image == true){ eltd_inline_style($title_subtitle_padding); } if($title_content_classes !== ""){ eltd_inline_style($title_area_content_background_color); } ?>>
                                <?php if($eltd_options['overlapping_content'] == 'yes') {?><div class="overlapping_content_margin"><?php } ?>
                                <?php if(($responsive_title_image == 'yes' && $show_title_image == true) || ($fixed_title_image == "yes" || $fixed_title_image == "yes_zoom") || ($responsive_title_image == 'no' && $title_image != "" && $fixed_title_image == "no" && $show_title_image == true)){ ?>
    							<div class="title_subtitle_holder_inner title_content_background" <?php eltd_inline_style($title_area_content_background_color); ?>>
    							<?php } ?>
    								<?php if($title_type != "breadcrumbs_title") { ?>

    									<?php if($title_graphics != ""){ ?>
    										<div class="title_graphics">
    											<img src=<?php echo esc_url($title_graphics); ?> alt="" class="title_graphics_img" <?php print $page_title_graphic_animation_data?>>
    										</div>
    									<?php } ?>



    									<?php if($subtitle_position=="above_title"){?>
    										<?php if(get_post_meta($id, "eltd_page_subtitle", true) != ""){
    												if ($subtitle_like_separator){?>
    												<span class="subtitle subtitle_like_separator <?php echo esc_attr($subtitle_in_box); ?>" <?php eltd_inline_style($subtitle_color); ?>><span class="span_subtitle_separator" <?php print $page_subtitle_animation_data; ?> > <?php echo do_shortcode($subtitle_shortcode); // XSS OK ?></span></span>
    												<?php }
    												else {
    											?>
    												<span class="subtitle" <?php eltd_inline_style($subtitle_color); ?>><span <?php print $page_subtitle_animation_data; ?> <?php eltd_inline_style($subtitle_span_style); ?>><?php echo wp_kses_post(get_post_meta($id, "eltd_page_subtitle", true)); ?></span></span>
    										<?php }
    										} ?>
    									<?php } ?>

                                        <?php if($title_separator == "yes" && $title_separator_position_vertical == "above"){
                                                if ($with_icon) {?>
                                                    <div  <?php print $page_title_separator_animation_data; ?>>
                                                    <?php echo do_shortcode('[no_separator_with_icon '. $separator_shortcode_params .' ]'); // XSS OK ?>
                                                    </div>
                                                <?php }
                                                else {?>
                                                    <span class="separator small <?php echo esc_attr($separator_title_position); ?>" <?php eltd_inline_style($title_separator_style); ?>  <?php print $page_title_separator_animation_data; ?>></span>
                                                <?php } ?>
                                        <?php } ?>




                                        <?php if($is_title_text_visible) {
                                                if ($subtitle_position == "next_to_title" && get_post_meta($id, "eltd_page_subtitle", true) != ""){?>
                                                    <h1<?php print $page_title_animation_data; if(get_post_meta($id, "eltd_page-title-color", true)) { ?> style="color:<?php echo esc_attr(get_post_meta($id, "eltd_page-title-color", true)); ?>" <?php } ?>><span <?php eltd_inline_style($title_span_style); ?>><?php echo wp_kses_post($title); ?></span></h1>
                                                    <span class="subtitle next_to_title" <?php eltd_inline_style($subtitle_color); ?>><span <?php print $page_subtitle_animation_data; ?> <?php eltd_inline_style($subtitle_span_style); ?>><?php echo wp_kses_post(get_post_meta($id, "eltd_page_subtitle", true)); ?></span></span>
                                            <?php }
                                                else{
                                                    if($title_like_separator){ ?>
                                                        <h1 class="title_like_separator <?php echo esc_attr($title_in_box); ?>" <?php print $page_title_animation_data; if(get_post_meta($id, "eltd_page-title-color", true)) { ?> style="color:<?php echo esc_attr(get_post_meta($id, "eltd_page-title-color", true)); ?>" <?php } ?>><?php echo do_shortcode($title_shortcode); // XSS OK ?></h1>
                                                <?php }
                                                    else {
                                                ?>

                                                <?php 

                                                //Get the subtitle after the : in the title
                                                $tmpSubTitle  = '';
                                                $tmpTitle  = wp_kses_post($title);

                                                $titleArray = explode(':', wp_kses_post($title));


                                                if (count($titleArray) > 1){
                                                    $tmpTitle = $titleArray[0] . ':';
                                                    $tmpSubTitle = $titleArray[1];
                                                }

                                                
                                                ?>
    										          <h1<?php print $page_title_animation_data; if(get_post_meta($id, "eltd_page-title-color", true)) { ?> style="color:<?php echo esc_attr(get_post_meta($id, "eltd_page-title-color", true)); ?>" <?php } ?>>
                                                      <span <?php eltd_inline_style($title_span_style); ?>><?php echo $tmpTitle; ?></span>
                                                        </h1>
                                                    <?php if ($tmpSubTitle != '')  { ?>
                                                    <h6<?php 
                                                    print $page_title_animation_data; 
                                                    if(get_post_meta($id, "eltd_page-title-color", true)) { ?> style="color:<?php echo esc_attr(get_post_meta($id, "eltd_page-title-color", true)); ?>" <?php } ?>>
                                                      <span <?php eltd_inline_style($title_span_style); ?>><?php echo $tmpSubTitle; ?></span>
                                                        </h6>
                                                  <?php } ?>
    									<?php } }
                                        } ?>




                                        <?php if($title_separator == "yes" && $title_separator_position_vertical == "below"){
                                                if ($with_icon) {?>
                                                    <div  <?php print $page_title_separator_animation_data; ?>>
                                                    <?php echo do_shortcode('[no_separator_with_icon '. $separator_shortcode_params .' ]'); // XSS OK ?>
                                                    </div>
                                                <?php }
                                                else {?>
                                                    <span class="separator small <?php echo esc_attr($separator_title_position); ?>" <?php eltd_inline_style($title_separator_style); ?> <?php print $page_title_separator_animation_data; ?>></span>
                                                <?php } ?>
                                        <?php } ?>



    								<?php if($subtitle_position=="below_title"){?>
    									<?php if(get_post_meta($id, "eltd_page_subtitle", true) != ""){
                                                if ($subtitle_like_separator){?>
                                                <span class="subtitle subtitle_like_separator <?php echo esc_attr($subtitle_in_box); ?>" <?php eltd_inline_style($subtitle_color); ?>><span class="span_subtitle_separator" <?php print $page_subtitle_animation_data; ?> > <?php echo do_shortcode($subtitle_shortcode); // XSS OK ?></span></span>
                                                <?php }
                                                else {
                                            ?>
    									        <span class="subtitle" <?php eltd_inline_style($subtitle_color); ?>><span <?php print $page_subtitle_animation_data; ?> <?php eltd_inline_style($subtitle_span_style); ?>><?php echo wp_kses_post(get_post_meta($id, "eltd_page_subtitle", true));  ?></span></span>
    									<?php }
                                        } ?>
    								<?php } ?>

    									<?php if (function_exists('eltd_custom_breadcrumbs') && $enable_breadcrumbs == "yes") { ?>
    										<div class="breadcrumb" <?php print $page_title_breadcrumbs_animation_data; ?>> <?php eltd_custom_breadcrumbs(); ?></div>
    									<?php } ?>

    								<?php } else { ?>

    									<div class="breadcrumb" <?php print $page_title_breadcrumbs_animation_data; ?>> <?php eltd_custom_breadcrumbs(); ?></div>

    								<?php } ?>
    							<?php if(($responsive_title_image == 'yes' && $show_title_image == true)  || ($fixed_title_image == "yes" || $fixed_title_image == "yes_zoom") || ($responsive_title_image == 'no' && $title_image != "" && $fixed_title_image == "no" && $show_title_image == true)){ ?>
    							</div>
    							<?php } ?>
                                    <?php if($eltd_options['overlapping_content'] == 'yes') {?></div><?php } ?>
    						</div>
    					</div>



    				</div>
    			</div>
		</div>
	</div>
<?php } ?>
<?php
	/* Return id for archive pages */
	if(is_category() || is_tag() || is_author()){
		$id = $archive_id;
	}
?>
