<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


ini_set( 'upload_max_size' , '64M' );

ini_set( 'post_max_size', '64M');

// Define site host
if (isset($_SERVER['HTTP_X_FORWARDED_HOST']) && !empty($_SERVER['HTTP_X_FORWARDED_HOST'])) {
    $hostname = $_SERVER['HTTP_X_FORWARDED_HOST'];
} else {
    $hostname = $_SERVER['HTTP_HOST'];
}

switch ($hostname) {
    case 'dev.dieste.com':
        define('WP_ENV', 'development');
        break;
    
    case '2016staging.dieste.com':
        define('WP_ENV', 'staging');
        break;
	
	case 'dieste.hangar.agency':
        define('WP_ENV', 'hangaragency');
        break;

    case 'www.dieste.com':
    case 'dieste.com':
    default: 
        define('WP_ENV', 'production');
}

// Load config file for current environment
include ABSPATH . '/wp-config.' . WP_ENV . '.php';

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
// define('DB_NAME', 'dieste_db');

/** MySQL database username */
// define('DB_USER', 'root');

/** MySQL database password */
// define('DB_PASSWORD', 'root');

/** MySQL hostname */
// define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i L}u%zs<hPK5uTZ`,XGFl6AKq[zK/HbT.)IoDv(-L,(&%ahG#Yn]pYSP5?TvsP<');
define('SECURE_AUTH_KEY',  'q2-p~-L`GCCJ%C(+Q9?3KlF:70h{!,^UJ-}-Lce~YE/m;kvBP&j_n/=tfBLl!WI[');
define('LOGGED_IN_KEY',    'kyUIH>M cMQyq>7#_Li&5o=w9|&C69F8l1qh@9IZ#6?1S&S8C$<;yqZN.{]UTlx0');
define('NONCE_KEY',        '=L9HB0^loE;FX?!gFEW2:r`%`>7t8rS1GOa|aQZSnLg=],IEMAUz+de%LjY% cJu');
define('AUTH_SALT',        'd{]uIq:r$G*^,1<F3TvD_fiL$N5_NKz2tTMRdWd j gpXGMC@|!.L%%bQ(<!f2-q');
define('SECURE_AUTH_SALT', 'Oc&&,X<xAh-7RLW}UU:w-u69}[I-ZN[2d4rg];@[_1mOV>BuX<Uq]8!d3BOOWN.T');
define('LOGGED_IN_SALT',   '63Z6RI[$sx{1G$dv$xZR[=o[|F^JM4FTkU+1!ObEj3&Ff>wi^|[eO*x6D#jj|Q7Y');
define('NONCE_SALT',       '+QbIcBj1tT n T-#e`8f=B=H#N,n60% -e+vYuaL!>K6F1+UJiPv3g/.Hz9s>VA#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
