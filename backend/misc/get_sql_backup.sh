#!/bin/sh

# Script for generate and copy the backup sql of the database of the actual project
#Author: Jorge L

#-i
pwd
ssh -i ../.vagrant/machines/default/virtualbox/private_key vagrant@dev.dieste.com "mysqldump -u root -proot -h localhost dieste_db > backup.sql"
scp -i ../.vagrant/machines/default/virtualbox/private_key vagrant@dev.dieste.com:/home/vagrant/backup.sql ../vagrant-data/modules/mysql/files/backup.sql
